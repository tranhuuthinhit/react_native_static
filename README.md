# Introduction
This is Satra application support user manager the Loyalty Card

# How to run

Clone the source code from this repo.

Follow the step for re-install components

Install `node_module`
````
yarn install
````

1. iOS

Goto the `ios` folder, and then run this command
````
pod install
````

2. Android

# Setup some components
## How to setup `react-native-maps`

#### 1. Installation
````terminal
yarn add react-native-maps@0.20.1
````

#### 2. Linking
##### iOS
Setup your `Podfile` with the content (found at /ios/Podfile as below):

````
source 'https://github.com/CocoaPods/Specs.git'

platform :ios, '8.0'

target 'satra_app' do
  # Fixes required for pod specs to work with rn 0.42
  react_native_path = "../node_modules/react-native"
  pod "yoga", :path => "#{react_native_path}/ReactCommon/yoga"
  pod "React", :path => react_native_path

  pod 'GoogleMaps'  # <~~ remove this line if you do not want to support GoogleMaps on iOS

end
````

And then run `pod install` while in the `ios` folder.

`If you want to use Google maps`. Update to `ios/satra_app/AppDelegate.m`:

````
@import GoogleMaps;
````
````
[GMSServices provideAPIKey:@"_YOUR_API_KEY_"];
````

````
#import "AppDelegate.h"

#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>

#import "RCCManager.h"
@import GoogleMaps; //add this line if you want to use Google Maps
@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  NSURL *jsCodeLocation;
  [GMSServices provideAPIKey:@"AIzaSyA6r31Xdtfy-YIPON83frsVCSffU8FdGZg"];
  
#ifdef DEBUG
  jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index.ios" fallbackResource:nil];
#else
  jsCodeLocation = [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
  
  
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  self.window.backgroundColor = [UIColor whiteColor];
  [[RCCManager sharedInstance] initBridgeWithBundleURL:jsCodeLocation launchOptions:launchOptions];
  
  return YES;
}

@end
````

Continue add `AirMaps.xcodeproj` to `Libraries` (found at `~/node_modules/react-native-maps/lib/ios/AirMaps.xcodeproj` as below).

And then, goto Build Phases, add `libAirMaps.a` to `Link Binary With Libraries`.

The last step, make sure added all header to `Header Search Paths`, and set `recursive`:

````
$(SRCROOT)/Pods
$(SRCROOT)/../node_modules/react-native-maps/lib/ios
$(SRCROOT)/../node_modules/react-native-maps/ios/AirMaps
````

##### Android

1. In your `android/app/build.gradle` add:

````
...
dependencies {
  ...
  compile project(':react-native-maps')
}
````

- If you've defined [project-wide properties](https://developer.android.com/studio/build/gradle-tips.html) (recommended) in your root build.gradle, this library will detect the presence of the following properties:

````
```groovy
buildscript {...}
allprojects {...}

/**
 + Project-wide Gradle configuration properties
 */
ext {
    compileSdkVersion   = 26
    targetSdkVersion    = 26
    buildToolsVersion   = "26.0.2"
    supportLibVersion   = "26.1.0"
    googlePlayServicesVersion = "11.8.0"
    androidMapsUtilsVersion = "0.5+"
}
```
````

- If you do not have project-wide properties defined and have a different play-services version than the one included in this library, use the following instead (switch 10.0.1 for the desired version):

````
...
dependencies {
    ...
    compile(project(':react-native-maps')){
        exclude group: 'com.google.android.gms', module: 'play-services-base'
        exclude group: 'com.google.android.gms', module: 'play-services-maps'
    }
    compile 'com.google.android.gms:play-services-base:10.0.1'
    compile 'com.google.android.gms:play-services-maps:10.0.1'
}
````

2. In your `android/settings.gradle` add:

````
...
include ':react-native-maps'
project(':react-native-maps').projectDir = new File(rootProject.projectDir, '../node_modules/react-native-maps/lib/android')
````

3. Specify your Google Maps API Key:

Add your API key to your manifest file (android\app\src\main\AndroidManifest.xml):

````
<application>
    <!-- You will only need to add this meta-data tag, but make sure it's a child of application -->
    <meta-data
      android:name="com.google.android.geo.API_KEY"
      android:value="Your Google maps API Key Here"/>
</application>
````

4. Add import `com.airbnb.android.react.maps.MapsPackage;` and `new MapsPackage()` in your `MainApplication.java`:

````
import com.airbnb.android.react.maps.MapsPackage;
...
@Override
     protected List<ReactPackage> getPackages() {
         return Arrays.<ReactPackage>asList(
                 new MainReactPackage(),
                 new MapsPackage()
         );
     }
````

5. Ensure that you have Google Play Services installed

- For Genymotion you can follow [these instructions](https://www.genymotion.com/help/desktop/faq/#google-play-services).
- For a physical device you need to search on Google for 'Google Play Services'. There will be a link that takes you to the Play Store and from there you will see a button to update it (do not search within the Play Store).