package net.youthdev.satramart.satramart;

import com.AlexanderZaytsev.RNI18n.RNI18nPackage;
import com.airbnb.android.react.maps.MapsPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.reactnativenavigation.NavigationApplication;
import net.youthdev.satramart.satramart.BuildConfig;
import com.agontuk.RNFusedLocation.RNFusedLocationPackage;

import java.util.Arrays;
import java.util.List;

public class MainApplication extends NavigationApplication {

  @Override
  public boolean isDebug() {
    return BuildConfig.DEBUG;
  }

  protected List getPackages() {
    //Add additional packages you require here
    //No need to add RNNPackage and MainReactPackage
    return Arrays.asList(
      new VectorIconsPackage(),
      new MapsPackage(),
      new RNI18nPackage(),
      new RNFusedLocationPackage()
    );
  }

  @Override
  public List createAdditionalReactPackages() {
    return getPackages();
  }

}
