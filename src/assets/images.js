const images = {
  bg: require('../assets/imgs/bg.png'),
  bg2: require('../assets/imgs/bg2.png'),
  logo: require('../assets/imgs/logo.png'),
  logoMap: require('../assets/imgs/logo_map.png'),
  noAvatar: require('../assets/imgs/noAvatar.png'),
  bonus: require('../assets/imgs/bonus.png'),
  card: require('../assets/imgs/card.png'),
  logoSatra: require('../assets/imgs/logoSatra.png'),
  logoSatraMart: require('../assets/imgs/logoSatraMart.png'),
  logoSatraFood: require('../assets/imgs/logoSatraFood.png')
}

export default images
