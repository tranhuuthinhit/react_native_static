import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  View,
  StyleSheet,
  TextInput
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

export default class ProfileInputRow extends Component {

  static propTypes = {
    iconName: PropTypes.string,
    text: PropTypes.string
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.width = width
  }

  render() {
    const { iconName, text } = this.props
    return (
      <View style={[styles.profileRow, { width: this.width }]}>
        <View style={styles.profileRowIconView}>
          <Icon name={iconName} size={25} color='#F6931D' />
        </View>
        <View style={[styles.profileRowTextView, { width: this.width - 60 }]}>
          <TextInput
            value={text}
            underlineColorAndroid='transparent'
          />
        </View>
      </View>
    )
  }
}

ProfileInputRow.defaultProps = {
  iconName: '',
  text: ''
}

const styles = StyleSheet.create({
  profileRow: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 60
  },
  profileRowIconView: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center'
  },
  profileRowTextView: {
    height: 60,
    borderColor: '#DFDFE4',
    justifyContent: 'center',
    borderBottomWidth: 0.5
  },
  profileRowText: {
    fontSize: 16,
    color: '#7F7E80'
  }
})