import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  View,
  StyleSheet,
  Text,
  Switch
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

export default class ProfileTextRowWithSwitch extends Component {

  static propTypes = {
    iconName: PropTypes.string,
    text: PropTypes.string,
    value: PropTypes.bool,
    onValueChange: PropTypes.func
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.width = width
  }

  render() {
    const { iconName, text, onValueChange, value } = this.props
    return (
      <View style={[styles.profileRow, { width: this.width }]}>
        <View style={styles.profileRowIconView}>
          <Icon name={iconName} size={25} color='#F6931D' />
        </View>
        <View style={[styles.profileRowTextView, {paddingRight: 25, alignItems: 'center', justifyContent: 'space-between', flexDirection: 'row', width: this.width - 60}]}>
          <View style={[styles.profileRowTextView]}>
            <Text style={styles.profileRowText}>{text}</Text>
          </View>
          <Switch
            onValueChange={(value) => onValueChange(value)}
            value={value}
          />
        </View>
      </View>
    )
  }
}

ProfileTextRowWithSwitch.defaultProps = {
  iconName: '',
  text: '',
  value: false,
  onValueChange: () => {}
}

const styles = StyleSheet.create({
  profileRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 60
  },
  profileRowIconView: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center'
  },
  profileRowTextView: {
    height: 60,
    borderColor: '#DFDFE4',
    justifyContent: 'center',
    borderBottomWidth: 0.5
  },
  profileRowText: {
    fontSize: 16,
    color: '#7F7E80'
  }
})