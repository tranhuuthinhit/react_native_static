import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  Image
} from 'react-native'
import * as THEME from '../constants/mainTheme'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class StoreCard extends Component {

  static propTypes = {
    storeName: PropTypes.string,
    storeAddress: PropTypes.string,
    storeImage: PropTypes.string,
    cardWidth: PropTypes.number,
    isIconDisplay: PropTypes.bool,
    iconName: PropTypes.string,
    iconColor: PropTypes.string,
    iconSize: PropTypes.number,
    iconOnPress: PropTypes.func,
    isDisplayAvatar: PropTypes.bool,
    backgroundColor: PropTypes.string
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.width = width
  }

  _renderIconAction() {
    const { iconName, iconColor, iconSize, iconOnPress, isIconDisplay } = this.props
    if(isIconDisplay) {
      return (
        <TouchableOpacity
          onPress={() => iconOnPress()}
        >
          <View>
            <Icon name={iconName} size={iconSize} color={iconColor} />
          </View>
        </TouchableOpacity>
      )
    }
  }

  _displayImageStore() {
    const { isDisplayAvatar, storeImage } = this.props
    if(isDisplayAvatar) {
      return (
        <Image source={{uri: storeImage}} style={styles.image} />
      )
    }
  }

  render() {
    const { storeName, storeAddress, cardWidth, backgroundColor } = this.props
    return (
      <View style={[styles.cardView, { width: cardWidth }]}>
        {this._displayImageStore()}
        <View style={[styles.storeInfo, { backgroundColor }]}>
          <View style={styles.storeNameAddress}>
            <Text style={styles.storeNameText}>{storeName.toUpperCase()}</Text>
            <Text style={styles.storeAddressText}>{storeAddress}</Text>
          </View>
          {this._renderIconAction()}
        </View>
      </View>
    )
  }
}

StoreCard.defaultProps = {
  storeName: '',
  storeAddress: '',
  storeImage: '',
  cardWidth: 0,
  isIconDisplay: false,
  iconName: '',
  iconColor: '',
  iconSize: 0,
  isDisplayAvatar: false,
  backgroundColor: THEME.PRIMARY_HEADER_COLOR,
  iconOnPress: () => {}
}

const styles = StyleSheet.create({
  cardView: {
    borderRadius: 10,
    overflow: 'hidden',
    marginTop: 10,
    marginBottom: 10
  },
  image: {
    resizeMode: 'cover',
    height: 150
  },
  storeInfo: {
    padding: 20,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
  },
  storeNameAddress: {
    flexDirection: 'column'
  },
  storeNameText: {
    color: '#FFFFFF',
    fontSize: 14,
    fontWeight: 'bold'
  },
  storeAddressText: {
    color: '#FFFFFF',
    fontSize: 12,
    marginTop: 5
  }
})