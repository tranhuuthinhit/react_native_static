import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  Image,
  View,
  StyleSheet
} from 'react-native'
import images from '../assets/images'

export default class LogoImage extends Component {

  static propTypes = {
    marginTop: PropTypes.number
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.inputWidth = width - 100
  }

  render() {
    const { marginTop } = this.props
    return (
      <View style={[styles.rowLogo, { marginTop }]}>
        <Image
          source={images.logoSatra}
          style={styles.logoSatra}
        />
      </View>
    )
  }
}

LogoImage.defaultProps = {
  marginTop: 0
}

const styles = StyleSheet.create({
  rowLogo: {
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 40
  },
  logoSatra: {
    resizeMode: 'contain',
    width: 300,
    height: 45
  }
})