import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  View,
  StyleSheet,
  TouchableOpacity,
  Text
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

export default class ProfileTextRowAction extends Component {

  static propTypes = {
    iconName: PropTypes.string,
    text: PropTypes.string,
    onPress: PropTypes.func
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.width = width
  }

  render() {
    const { iconName, text, onPress } = this.props
    return (
      <TouchableOpacity
        onPress={() => onPress()}
      >
        <View style={[styles.profileRow, { width: this.width }]}>
          <View style={styles.profileRowIconView}>
            <Icon name={iconName} size={25} color='#F6931D' />
          </View>
          <View style={[styles.profileRowTextView, { width: this.width - 60 }]}>
            <Text style={styles.profileRowText}>{text}</Text>
            <Icon name='ios-arrow-forward' size={25} color='#7F7E80' />
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

ProfileTextRowAction.defaultProps = {
  iconName: '',
  text: '',
  onPress: () => {}
}

const styles = StyleSheet.create({
  profileRow: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 60
  },
  profileRowIconView: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center'
  },
  profileRowTextView: {
    flexDirection: 'row',
    height: 60,
    borderColor: '#DFDFE4',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomWidth: 0.5,
    paddingRight: 16
  },
  profileRowText: {
    fontSize: 16,
    color: '#7F7E80'
  }
})