import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  Image
} from 'react-native'
import * as THEME from '../constants/mainTheme'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class PromotionCard extends Component {

  static propTypes = {
    title: PropTypes.string,
    promotionImage: PropTypes.string,
    cardWidth: PropTypes.number,
    OnPress: PropTypes.func
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.width = width
  }

  render() {
    const { title, promotionImage, cardWidth, OnPress } = this.props
    return (
      <TouchableOpacity
        OnPress={() => OnPress()}
      >
        <View style={[styles.cardView, { width: cardWidth }]}>
          <Image source={{uri: promotionImage}} style={styles.image} />
          <View style={styles.storeInfo}>
            <View style={[styles.storeNameAddress, { maxWidth: cardWidth - 80}]}>
              <Text style={styles.storeNameText}>{title.toUpperCase()}</Text>
            </View>
            <View style={styles.iconView}>
              <Icon name='chevron-right' size={25} color='#FFFFFF' />
            </View>
          </View>
        </View>
      </TouchableOpacity>
    )
  }
}

PromotionCard.defaultProps = {
  title: '',
  promotionImage: '',
  cardWidth: 0,
  OnPress: () => {}
}

const styles = StyleSheet.create({
  cardView: {
    borderRadius: 10,
    overflow: 'hidden',
    marginTop: 10,
    marginBottom: 10
  },
  image: {
    resizeMode: 'cover' ,
    height: 200
  },
  storeInfo: {
    backgroundColor: THEME.PRIMARY_HEADER_COLOR,
    padding: 20,
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center'
  },
  storeNameAddress: {
    flexDirection: 'column'
  },
  storeNameText: {
    color: '#FFFFFF',
    fontSize: 16,
    fontWeight: 'bold',
    flexWrap: 'wrap'
  },
  iconView: {
    marginLeft: 20
  }
})