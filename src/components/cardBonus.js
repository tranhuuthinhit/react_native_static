import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  Image,
  View,
  StyleSheet
} from 'react-native'
import images from '../assets/images'

export default class CardBonus extends Component {

  static propTypes = {
    marginTop: PropTypes.number
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.inputWidth = width - 100
  }

  render() {
    const { marginTop } = this.props
    return (
      <View style={styles.rowCardBonus}>
        <Image source={images.card} style={[styles.cardImage, { marginTop }]} />
      </View>
    )
  }
}

CardBonus.defaultProps = {
  marginTop: 0
}

const styles = StyleSheet.create({
  rowCardBonus: {
    alignItems: 'center'
  },
  cardImage: {
    width: 300,
    height: 170,
    resizeMode: 'contain',
    marginTop: 20
  }
})