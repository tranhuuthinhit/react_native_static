import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  View,
  StyleSheet,
  TouchableOpacity,
  Text
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

export default class RadioBox extends Component {

  static propTypes = {
    iconName: PropTypes.string,
    label: PropTypes.string,
    value: PropTypes.bool,
    onPress: PropTypes.func
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.width = width
  }

  render() {
    const { iconName, label, value, onPress } = this.props
    return (
      <TouchableOpacity
        disabled={value}
        onPress={() => onPress(value)}
      >
        <View style={styles.actionGender}>
          <Icon name={iconName} size={25} color='#F6931D' />
          <Text style={styles.profileRowText}>{label}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

RadioBox.defaultProps = {
  iconName: '',
  label: '',
  value: false,
  onPress: () => {}
}

const styles = StyleSheet.create({
  actionGender: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginRight: 20
  },
  profileRowText: {
    fontSize: 16,
    color: '#7F7E80',
    marginLeft: 5
  }
})