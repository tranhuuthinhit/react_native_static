import { StyleSheet, Dimensions } from 'react-native'
const { width, height } = Dimensions.get('window')

const optionStyle = {
  flex: 0,
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  paddingVertical: 10,
  paddingHorizontal: 10,
  borderBottomWidth: 1,
  borderBottomColor: '#EEEEEE'
}

const optionTextStyle = {
  flex: 1,
  textAlign: 'left',
  color: '#7F7E80',
  fontSize: 14
}

export default StyleSheet.create({
  overlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: '#000000',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
    opacity: 0.7
  },
  container: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'transparent',
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 2
  },
  titleTextStyle: {
    flex: 0,
    color: '#FFFFFF',
    fontSize: 20,
    marginBottom: 15
  },
  listContainer: {
    flex: 1,
    width: width * 0.8,
    maxHeight: height * 0.5,
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    marginBottom: 15
  },
  cancelContainer: {
    flex: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  cancelButton: {
    flex: 0,
    width: width * 0.8,
    backgroundColor: '#F59220',
    paddingVertical: 10,
    paddingHorizontal: 20,
    borderRadius: 5
  },
  cancelButtonText: {
    textAlign: 'center',
    fontSize: 14,
    color: '#FFFFFF'
  },
  filterTextInputContainer: {
    borderBottomWidth: 1,
    borderBottomColor: '#CCCCCC'
  },
  filterTextInput: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    flex: 0,
    height: 50
  },
  categoryStyle: {
    ...optionStyle
  },
  categoryTextStyle: {
    ...optionTextStyle,
    color: '#999999',
    fontStyle: 'italic',
    fontSize: 16
  },
  optionStyle: {
    ...optionStyle
  },
  optionStyleLastChild: {
    borderBottomWidth: 0
  },
  optionTextStyle: {
    ...optionTextStyle
  },
  selectedOptionStyle: {
    ...optionStyle
  },
  selectedOptionStyleLastChild: {
    borderBottomWidth: 0
  },
  selectedOptionTextStyle: {
    ...optionTextStyle
  },
  noResults: {
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    paddingHorizontal: 10,
    marginTop: 20
  },
  noResultsText: {
    flex: 1,
    textAlign: 'center',
    color: '#CCCCCC',
    fontStyle: 'italic',
    fontSize: 14
  }
})
