import React from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Platform,
  StyleSheet
} from 'react-native'

export default class ElevatedView extends React.Component {

  static propTypes = {
    elevation: PropTypes.number,
    style: PropTypes.array,
    children: PropTypes.object
  }

  render() {
    const { elevation, style, ...otherProps } = this.props

    if (Platform.OS === 'android') {
      return (
        <View elevation={3} style={[styles.androidView, style ]} {...otherProps}>
          {this.props.children}
        </View>
      )
    }

    const iosShadowElevation = {
      shadowOpacity: 0.0015 * elevation + 0.18,
      shadowRadius: 0.54 * elevation,
      shadowOffset: {
        height: 0.6 * elevation
      }
    }

    return (
      <View style={[iosShadowElevation, style]} {...otherProps}>
        {this.props.children}
      </View>
    )
  }
}

ElevatedView.defaultProps = {
  elevation: 0
}

const styles = StyleSheet.create({
  androidView: {
    backgroundColor: 'white'
  }
})