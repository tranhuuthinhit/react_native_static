import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity
} from 'react-native'

export default class MainButton extends Component {

  static propTypes = {
    text: PropTypes.string,
    buttonWidth: PropTypes.number,
    backgroundColor: PropTypes.string,
    onPress: PropTypes.func
  }

  render() {
    const { text, onPress, buttonWidth, backgroundColor } = this.props
    return (
      <TouchableOpacity
        onPress={() => onPress()}
      >
        <View style={[styles.buttonView, { width: buttonWidth, backgroundColor}]}>
          <Text style={styles.buttonText}>{text}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

MainButton.defaultProps = {
  text: 'Button',
  buttonWidth: 0,
  onPress: () => {}
}

const styles = StyleSheet.create({
  buttonView: {
    height: 40,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 13
  }
})