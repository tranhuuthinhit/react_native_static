import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  View,
  StyleSheet,
  TouchableOpacity,
  Text
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

export default class CheckBox extends Component {

  static propTypes = {
    iconName: PropTypes.string,
    disabled: PropTypes.bool,
    label: PropTypes.string,
    value: PropTypes.bool,
    onPress: PropTypes.func
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.width = width
  }

  render() {
    const { iconName, label, value, onPress, disabled } = this.props
    return (
      <TouchableOpacity
        disabled={disabled}
        onPress={() => onPress(value)}
      >
        <View style={[styles.profileRow]}>
          <Icon name={iconName} size={20} color='#F6931D' />
          <Text style={styles.profileRowText}>{label}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

CheckBox.defaultProps = {
  iconName: '',
  label: '',
  value: false,
  disabled: false,
  onPress: () => {}
}

const styles = StyleSheet.create({
  profileRow: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10
  },
  profileRowText: {
    fontSize: 12,
    color: '#7F7E80',
    marginLeft: 10
  }
})