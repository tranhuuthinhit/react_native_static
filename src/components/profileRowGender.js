import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  View,
  StyleSheet
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'
import I18n from '../utils/i18n'
import RadioBox from './radioBox'

export default class ProfileTextRowGender extends Component {

  static propTypes = {
    rowIcon: PropTypes.string,
    sex: PropTypes.string,
    callback: PropTypes.func,
    isEnableTouch: PropTypes.bool
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.width = width
    this.state = {
      value: false
    }
  }

  componentWillMount() {
    const { sex } = this.props
    if(sex === 'male') {
      this.setState({value: true})
    }
  }

  _handleCheckbox(sex) {
    const { isEnableTouch, callback } = this.props
    if(isEnableTouch) {
      this.setState({value: !this.state.value})
      callback(sex)
    }
  }

  render() {
    const { rowIcon } = this.props
    const { value } = this.state
    return (
      <View style={[styles.profileRow, { width: this.width }]}>
        <View style={styles.profileRowIconView}>
          <Icon name={rowIcon} size={25} color='#F6931D' />
        </View>
        <View style={[styles.profileRowTextView, { width: this.width - 60 }]}>
          <RadioBox
            iconName={value ? 'md-radio-button-on' : 'md-radio-button-off'}
            label={I18n('myInformationScreen.sex.male')}
            value={value}
            onPress={() => this._handleCheckbox('male')}
          />

          <RadioBox
            iconName={value ? 'md-radio-button-off' : 'md-radio-button-on'}
            label={I18n('myInformationScreen.sex.female')}
            value={!value}
            onPress={() => this._handleCheckbox('female')}
          />
        </View>
      </View>
    )
  }
}

ProfileTextRowGender.defaultProps = {
  rowIcon: '',
  isEnableTouch: false,
  sex: 'male'
}

const styles = StyleSheet.create({
  profileRow: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 60
  },
  profileRowIconView: {
    width: 60,
    height: 60,
    justifyContent: 'center',
    alignItems: 'center'
  },
  profileRowTextView: {
    height: 60,
    borderColor: '#DFDFE4',
    justifyContent: 'flex-start',
    borderBottomWidth: 0.5,
    flexDirection: 'row',
    alignItems: 'center'
  }
})