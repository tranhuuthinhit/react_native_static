import I18n from '../i18n'

export default function i18n(key) {
  return I18n.t(key)
}