function FETCH_DATA(path, method, headers, body) {
  return new Promise(function (resolve, reject) {
    timeoutFecth(30000, fetch(path, method, headers, body))
      .then((response) => {
        response.json()
      })
      .then(responseJson => {
        resolve(responseJson)
      })
      .catch((error) => {
        reject(error)
      })
  })
}

function timeoutFecth(ms, promise) {
  return new Promise((resolve, reject) => {
    const timeout = setTimeout(() => {
      reject(new Error('Timeout'))
    }, ms)
    promise
      .then(response => {
        clearTimeout(timeout)
        resolve(response)
      })
      .catch(err => {
        clearTimeout(timeout)
        reject(err)
      })
  })
}

export {
  FETCH_DATA
}