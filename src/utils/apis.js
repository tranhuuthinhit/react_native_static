import { FETCH_DATA } from './fetch'

function getData() {
  const path = 'http://spare-staff-backend.dev3.youthdev.net/states/'
  const method = 'GET'
  const headers = {}
  const body = {}
  return FETCH_DATA(path, method, headers, body)
}

export {
  getData
}