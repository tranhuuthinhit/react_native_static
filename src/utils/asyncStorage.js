import {
  AsyncStorage
} from 'react-native'
const APP_INFO = 'APP_INFO'
const USER_INFO = 'USER_INFO'
const LOGIN_STATUS = 'LOGIN_STATUS'

function getAppInfo() {
  return new Promise(async (resolve, reject) => {
    try {
      await AsyncStorage.getItem(APP_INFO)
        .then((value) => {
          resolve(JSON.parse(value))
        })
        .catch((error) => {
          reject(error)
        })
    } catch(error) {
      reject(error)
    }
  })
}

async function setAppInfo(appInfo) {
  try {
    await AsyncStorage.setItem(APP_INFO, JSON.stringify(appInfo))
    console.log('Set AppInfo successfull')
  } catch(error) {
    console.log('Set AppInfo error: ', error)
  }
}

function getUserInfo() {
  return new Promise(async (resolve, reject) => {
    try {
      await AsyncStorage.getItem(USER_INFO)
        .then((value) => {
          resolve(JSON.parse(value))
        })
        .catch((error) => {
          reject(error)
        })
    } catch(error) {
      reject(error)
    }
  })
}

async function setUserInfo(userInfo) {
  try {
    await AsyncStorage.setItem(USER_INFO, JSON.stringify(userInfo))
    console.log('Set UserInfo successfull')
  } catch(error) {
    console.log('Set UserInfo error: ', error)
  }
}

function clear() {
  return new Promise(async (resolve, reject) => {
    try {
      await AsyncStorage.clear().then(() => {
        resolve('Cleared')
      })
    } catch (error) {
      reject(error)
    }
  })
}

function getLoginStatus() {
  return new Promise(async (resolve, reject) => {
    try {
      await AsyncStorage.getItem(LOGIN_STATUS)
        .then((value) => {
          resolve(JSON.parse(value))
        })
        .catch((error) => {
          reject(error)
        })
    } catch(error) {
      reject(error)
    }
  })
}

async function setLoginStatus(status) {
  try {
    await AsyncStorage.setItem(LOGIN_STATUS, JSON.stringify(status))
    console.log('Set LoginStatus successfull')
  } catch(error) {
    console.log('Set LoginStatus error: ', error)
  }
}

export {
  setAppInfo,
  getAppInfo,
  setUserInfo,
  getUserInfo,
  getLoginStatus,
  setLoginStatus,
  clear
}