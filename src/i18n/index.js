import I18n from 'react-native-i18n'
import isPlainObject from 'lodash/isPlainObject'
import languages from './locales/languages.json'

I18n.fallbacks = true
I18n.translations = languages

const tOrig = I18n.t
I18n.t = function(key, params = {}) {
  const res = tOrig.call(this, key, params)

  if (isPlainObject(res)) {
    return function(k, p = {}) {
      return I18n.t(`${key}.${k}`, { ...params, ...p })
    }
  }
  return res
}

export default I18n