import * as types from './actionTypes'
import Immutable from 'seamless-immutable'


const initialState = Immutable({
  root: undefined,
  appInfo: {
    deviceLanguage: ''
  }
})

export default function root(state = initialState, action = {}) {
  switch (action.type) {
    case types.ROOT_CHANGED:
      return {
        ...state,
        root: action.root
      }
    case types.SAVE_APP_INFO_TO_ROOT_STORE:
      return {
        ...state,
        appInfo: action.appInfo
      }
    default:
      return state
  }
}
