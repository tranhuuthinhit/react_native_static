import * as actions from './actions'

function appInitialized() {
  return (dispatch) => {
    dispatch(actions.changeAppRoot('splashScreen'))
  }
}

function updateAppRoot(root) {
  return (dispatch) => {
    dispatch(actions.changeAppRoot(root))
  }
}

function saveAppInfoToRootState(appInfo) {
  return (dispatch) => {
    dispatch(actions.saveAppInfoToRootState(appInfo))
  }
}

export {
  appInitialized,
  updateAppRoot,
  saveAppInfoToRootState
}