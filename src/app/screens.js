import { Navigation } from 'react-native-navigation'
import * as ROUTE from '../constants/routes'
import SplashScreen from '../features/splashScreen'
import SignIn from '../features/auth/screens/signIn'
import SignUp from '../features/auth/screens/signUp'
import Home from '../features/home'
import Profile from '../features/user/screens/myProfile'
import About from '../features/about'
import Term from '../features/term'
import Privacy from '../features/privacy'
import Setting from '../features/setting'
import Stores from '../features/stores/screens'
import Promotion from '../features/promotion'
import MyInformation from '../features/user/screens/myInformation'
import UpdateInformation from '../features/user/screens/updateInformation'
import MyHistory from '../features/history'
import StoresDirection from '../features/stores/screens/direction'
import AllStores from '../features/stores/screens/allStores'
import User from '../features/user/screens/index'
import ChangePassword from '../features/user/screens/changePassword'

export default (store, Provider) => {
  Navigation.registerComponent(ROUTE.SPLASHSCREEN, () => SplashScreen, store, Provider)
  Navigation.registerComponent(ROUTE.SIGN_IN, () => SignIn, store, Provider)
  Navigation.registerComponent(ROUTE.SIGN_UP, () => SignUp, store, Provider)
  Navigation.registerComponent(ROUTE.HOME, () => Home, store, Provider)
  Navigation.registerComponent(ROUTE.PROFILE, () => Profile, store, Provider)
  Navigation.registerComponent(ROUTE.ABOUT, () => About, store, Provider)
  Navigation.registerComponent(ROUTE.PRIVACY, () => Privacy, store, Provider)
  Navigation.registerComponent(ROUTE.TERM, () => Term, store, Provider)
  Navigation.registerComponent(ROUTE.SETTING, () => Setting, store, Provider)
  Navigation.registerComponent(ROUTE.STORES, () => Stores, store, Provider)
  Navigation.registerComponent(ROUTE.PROMOTION, () => Promotion, store, Provider)
  Navigation.registerComponent(ROUTE.MY_INFORMATION, () => MyInformation, store, Provider)
  Navigation.registerComponent(ROUTE.MY_HISTORY, () => MyHistory, store, Provider)
  Navigation.registerComponent(ROUTE.UPDATE_INFORMATION, () => UpdateInformation, store, Provider)
  Navigation.registerComponent(ROUTE.STORE_DIRECTION, () => StoresDirection, store, Provider)
  Navigation.registerComponent(ROUTE.ALL_STORES, () => AllStores, store, Provider)
  Navigation.registerComponent(ROUTE.USER, () => User, store, Provider)
  Navigation.registerComponent(ROUTE.CHANGE_PASSWORD, () => ChangePassword, store, Provider)
}