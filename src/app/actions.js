import * as types from './actionTypes'

function changeAppRoot(root) {
  return {
    type: types.ROOT_CHANGED,
    root
  }
}

function saveAppInfoToRootState(appInfo) {
  return {
    type: types.SAVE_APP_INFO_TO_ROOT_STORE,
    appInfo
  }
}

export {
  changeAppRoot,
  saveAppInfoToRootState
}