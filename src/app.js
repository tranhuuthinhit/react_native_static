import { Component } from 'react'
import { Provider } from 'react-redux'
import { Navigation } from 'react-native-navigation'
import registerScreens from './app/screens'
import * as appActions from './app/services'
import * as ROUTES from './constants/routes'
import I18n from './utils/i18n'
import { store } from './reducers'
import Icon from 'react-native-vector-icons/Ionicons'

registerScreens(store, Provider)

export default class App extends Component {

  constructor(props) {
    super(props)
    store.subscribe(this.onStoreUpdate.bind(this))
    store.dispatch(appActions.appInitialized())
  }

  async prepareIcons() {
    const icons = await Promise.all([
      Icon.getImageSource('md-home', 30, '#7F7E80'),
      Icon.getImageSource('ios-person', 30, '#7F7E80'),
      Icon.getImageSource('ios-basket', 30, '#7F7E80'),
      Icon.getImageSource('ios-map', 30, '#7F7E80')
    ])
    const [home, profile, promotion, maps] = icons
    return { home, profile, promotion, maps }
  }

  onStoreUpdate() {
    const { root } = store.getState().root
    if (this.currentRoot !== root) {
      this.currentRoot = root
      this.startApp(root)
    }
  }

  async startApp(root) {

    const icons = await this.prepareIcons()

    const homeTab = {
      label: I18n('homeScreen.lable'),
      icon: icons.home,
      screen: ROUTES.HOME,
      title: I18n('homeScreen.title')
    }

    const userTab = {
      label: I18n('profileScreen.lable'),
      icon: icons.profile,
      screen: ROUTES.USER,
      title: I18n('profileScreen.title')
    }

    const promotionTab = {
      label: I18n('promotionScreen.lable'),
      icon: icons.promotion,
      screen: ROUTES.PROMOTION,
      title: I18n('promotionScreen.title')
    }

    const storesTab = {
      label: I18n('storeScreen.lable'),
      icon: icons.maps,
      screen: ROUTES.STORES,
      title: I18n('nearbyStore.title')
    }

    const tabsStyle = {
      tabBarButtonColor: '#7F7E80',
      tabBarLabelColor: '#7F7E80',
      tabBarSelectedLabelColor: '#F59220',
      tabBarSelectedButtonColor: '#F59220',
      initialTabIndex: 0
    }

    const appStyle = {
      tabBarButtonColor: '#7F7E80',
      tabBarLabelColor: '#7F7E80',
      tabBarSelectedLabelColor: '#F59220',
      tabBarSelectedButtonColor: '#F59220',
      hideBackButtonTitle: true,
      forceTitlesDisplay: true
    }

    switch(root) {
      case 'splashScreen':
        Navigation.startSingleScreenApp({
          screen: {
            screen: ROUTES.SPLASHSCREEN,
            title: I18n('splashScreen.title')
          }
        })
        return
      case 'home':
        Navigation.startTabBasedApp({
          tabs: [
            homeTab,
            userTab,
            promotionTab,
            storesTab
          ],
          tabsStyle,
          appStyle
        })
        return
      default:
        console.log('Not root found')
    }
  }
}