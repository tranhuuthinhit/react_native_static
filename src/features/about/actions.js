import * as mainActions from '../../app/actions'

function nextScreen() {
  return dispatch => {
    const initialView = 'signIn'
    dispatch(mainActions.changeAppRoot(initialView))
  }
}

export {
  nextScreen
}