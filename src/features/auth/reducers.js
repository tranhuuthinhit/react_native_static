import * as types from './actionTypes'
import Immutable from 'seamless-immutable'

const initialState = Immutable({
  isAuthencated: false,
  isPasswordDisplay: false,
  userProfile: {
    fullname: '',
    identityCard: '',
    birthday: '',
    email: '',
    phone: '',
    sex: ''
  },
  signUpError: false,
  signInError: false,
  checkAccountError: false,

  signUpErrorMessage: '',
  signInErrorMessage: '',
  checkAccountErrorMessage: ''
})

export default function auth(state = initialState, action = {}) {
  switch(action.type) {
    case types.SIGN_UP:
      return {
        ...state
      }
    case types.SIGN_UP_SUCCESS:
      return {
        ...state,
        isAuthencated: true,
        userProfile: action.userProfile
      }
    case types.SIGN_UP_FAILURE:
      return {
        ...state,
        signUpError: true,
        signUpErrorMessage: action.error
      }
    case types.SIGN_IN:
      return {
        ...state
      }
    case types.SIGN_IN_SUCCESS:
      return {
        isAuthencated: true,
        userProfile: action.userProfile
      }
    case types.SIGN_IN_FAILURE:
      return {
        ...state,
        signInError: true,
        signInErrorMessage: action.error
      }
    case types.CHECK_ACCOUNT: {
      return {
        ...state
      }
    }
    case types.CHECK_ACCOUNT_SUCCESS:
      return {
        ...state,
        isPasswordDisplay: true
      }
    case types.CHECK_ACCOUNT_FAILURE: {
      return {
        ...state,
        checkAccountError: true,
        checkAccountErrorMessage: action.error,
        isPasswordDisplay: false
      }
    }
    case types.RESET_ERROR:
      return {
        ...state,
        signUpError: false,
        signInError: false,
        checkAccountError: false,
        signUpErrorMessage: '',
        signInErrorMessage: '',
        checkAccountErrorMessage: ''
      }
    case types.LOG_OUT:
      return initialState
    default:
      return state
  }
}