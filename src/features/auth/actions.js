import * as types from './actionTypes'

function signUp() {
  return {
    type: types.SIGN_UP
  }
}

function signUpSuccess(userProfile) {
  return {
    type: types.SIGN_UP_SUCCESS,
    userProfile
  }
}

function signUpFailure(error) {
  return {
    type: types.SIGN_UP_FAILURE,
    error
  }
}

function signIn() {
  return {
    type: types.SIGN_IN
  }
}

function signInSuccess(userProfile) {
  return {
    type: types.SIGN_IN_SUCCESS,
    userProfile
  }
}

function signInFailure(error) {
  return {
    type: types.SIGN_IN_FAILURE,
    error
  }
}

function logOut() {
  return {
    type: types.LOG_OUT
  }
}

function checkAccount() {
  return {
    type: types.CHECK_ACCOUNT
  }
}

function checkAccountSuccess() {
  return {
    type: types.CHECK_ACCOUNT_SUCCESS
  }
}

function checkAccountFailure(error) {
  return {
    type: types.CHECK_ACCOUNT_FAILURE,
    error
  }
}

function resetError() {
  return {
    type: types.RESET_ERROR
  }
}

export {
  signIn,
  signInSuccess,
  signInFailure,
  logOut,
  signUp,
  signUpSuccess,
  signUpFailure,
  checkAccount,
  checkAccountSuccess,
  checkAccountFailure,
  resetError
}