import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  View,
  StatusBar,
  Alert,
  Text,
  TouchableOpacity
} from 'react-native'
import styles from './styles'
import { connect } from 'react-redux'
import navigationStyles from '../../../constants/navigationStyles'
import {
  authenticate,
  checkAccount,
  resetError
} from '../services'
import MainTextInput from '../../../components/mainTextInput'
import LogoImage from '../../../components/logoImage'
import CardBonus from '../../../components/cardBonus'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import I18n from '../../../utils/i18n'
import MainButton from '../../../components/mainButton'
import * as ROUTES from '../../../constants/routes'

export class SignIn extends Component {

  static propTypes = {
    navigator: PropTypes.object,
    dispatchAuthenticate: PropTypes.func,
    dispatchCheckAccount: PropTypes.func,
    dispatchResetError: PropTypes.func,
    isPasswordDisplay: PropTypes.bool,
    signInError: PropTypes.bool,
    checkAccountError: PropTypes.bool,
    signInErrorMessage: PropTypes.string,
    checkAccountErrorMessage: PropTypes.string
  }

  static navigatorStyle = {
    ...navigationStyles.signIn
  }

  constructor(props) {
    super(props)
    const { width, height } = Dimensions.get('window')
    this.width = width
    this.height = height
    this.state = {
      username: 'test',
      password: '123'
    }
  }

  componentDidUpdate(prevProps) {
    const {
      signInError,
      signInErrorMessage,
      checkAccountError,
      checkAccountErrorMessage
    } = this.props

    if (!prevProps.signInError && signInError) {
      this._displayAlert(I18n('errorTitle'), signInErrorMessage)
    }

    if (!prevProps.checkAccountError && checkAccountError) {
      this._displayAlert(I18n('errorTitle'), checkAccountErrorMessage)
    }
  }

  _displayAlert(title, message) {
    Alert.alert(title,message,
      [{text: 'OK', onPress: () => this.props.dispatchResetError()}]
    )
  }

  _onChangeText(key, value) {
    this.setState({
      [key]: value
    })
  }

  _checkAccount() {
    const { username } = this.state
    this.props.dispatchCheckAccount(username)
  }

  _signIn() {
    const { username, password } = this.state
    const { isPasswordDisplay } = this.props

    if(!isPasswordDisplay) {
      this.props.dispatchCheckAccount(username)
    } else {
      const userInfo = {
        username,
        password
      }

      this.props.dispatchAuthenticate(userInfo)
    }
  }

  _renderPasswordField() {
    const { isPasswordDisplay } = this.props
    if(isPasswordDisplay) {
      return (
        <MainTextInput
          placeholder={I18n('signInScreen.placeholderPassword')}
          placeholderTextColor='#76805D'
          backgroundColor='#8BC540'
          type='password'
          onChangeText={(key, value) => this._onChangeText(key, value)}
          value={this.state.password}
          secureTextEntry={true}
          marginTop={10}
        />
      )
    }
    return null
  }

  _goToOtherScreen(screen, title) {
    this.props.navigator.push({
      screen,
      title,
      backButtonTitle: ''
    })
  }

  render() {
    const { isPasswordDisplay } = this.props
    return (
      <KeyboardAwareScrollView
        style={styles.keyboardAwareScrollView}
      >
        <StatusBar barStyle='light-content' />
        <View style={[styles.rootContainer, { height: this.height }]}>
          <LogoImage marginTop={40} />
          <CardBonus marginTop={30} />
          <View style={[styles.rowForm]}>
            <MainTextInput
              disable={!isPasswordDisplay}
              placeholder={I18n('signInScreen.placeholderEmail')}
              placeholderTextColor='#76805D'
              backgroundColor='#8BC540'
              type='username'
              onChangeText={(key, value) => this._onChangeText(key, value)}
              value={this.state.username}
            />
            {this._renderPasswordField()}
          </View>
          <View style={styles.rowButton}>
            <MainButton
              text={I18n('signInScreen.buttonTextSignIn')}
              backgroundColor='#ED1D25'
              buttonWidth={this.width - 100}
              onPress={() => this._signIn()}
            />
            <TouchableOpacity
              onPress={() => this._goToOtherScreen(ROUTES.SIGN_UP, I18n('signUpScreen.title'))}
            >
              <Text style={styles.registerButton}>{I18n('signInScreen.buttonTextSignUp')}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </KeyboardAwareScrollView>
    )
  }
}

const mapDispatchToProps = {
  dispatchAuthenticate: userInfo => authenticate(userInfo),
  dispatchCheckAccount: username => checkAccount(username),
  dispatchResetError: () => resetError()
}

const mapStateToProps = (state) => ({
  isPasswordDisplay: state.auth.isPasswordDisplay,
  signInError: state.auth.signInError,
  checkAccountError: state.auth.checkAccountError,
  signInErrorMessage: state.auth.signInErrorMessage,
  checkAccountErrorMessage: state.auth.checkAccountErrorMessage
})

export default connect(mapStateToProps, mapDispatchToProps)(SignIn)