import { StyleSheet, Dimensions } from 'react-native'
const { width } = Dimensions.get('window')

const styles = StyleSheet.create({
  keyboardAwareScrollView: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  rootContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'stretch'
  },
  rootContainerSignUp: {
    flex: 1,
    justifyContent: 'flex-start',
    paddingBottom: 20
  },
  rowForm: {
    width,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30
  },
  rowFormSignUp: {
    width,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 5
  },
  rowInput: {
    borderRadius: 5,
    backgroundColor: '#C9E1A6',
    padding: 12
  },
  rowButton: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20
  },
  registerButton: {
    fontSize: 13,
    marginTop: 30,
    color: '#F7941C'
  },
  datePicker: {
    width: width- 30,
    backgroundColor: '#E4E2E5',
    borderRadius: 5
  }
})

export default styles