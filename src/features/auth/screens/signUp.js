import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  StatusBar,
  View
} from 'react-native'
import styles from './styles'
import { connect } from 'react-redux'
import navigationStyles from '../../../constants/navigationStyles'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import I18n from '../../../utils/i18n'
import SignUpTextInput from '../components/signUpTextInput'
import SingUpRowTitle from '../components/signUpRowTitle'
import ModalFilterPicker from '../../../components/modalFilterPicker'
import SignUpDropDown from '../components/signUpDropDown'
import MainButton from '../../../components/mainButton'
import DatePicker from 'react-native-datepicker'
import SignUpDatePicker from '../components/signUpDatePicker'
import moment from 'moment'


export class SignUp extends Component {

  static propTypes = {
  }

  static navigatorStyle = {
    ...navigationStyles.signUp
  }

  constructor(props) {
    super(props)
    const { width, height } = Dimensions.get('window')
    this.width = width
    this.height = height
    this.optionsSex = [
      {
        key: 'Nam',
        label: 'Nam'
      },
      {
        key: 'Nữ',
        label: 'Nữ'
      }
    ]
    this.state = {
      email: '',
      password: '',
      firstName: '',
      lastName: '',
      phoneNumber: '',
      identityCard: '',
      birthday: '',
      sex: '',
      isDisplayDropDown: false,
      dropDownData: [],
      pickerKey: ''
    }
  }

  _onChangeText(key, value) {
    this.setState({
      [key]: value
    })
  }

  _showDropDown(typeModal, dropDownData) {
    this.setState({
      typeModal,
      dropDownData,
      isDisplayDropDown: true
    })
  }

  _onSelected(picked) {
    const { typeModal } = this.state
    this.setState({
      [typeModal]: picked,
      isDisplayDropDown: false
    })
  }

  _onCancel() {
    const { typeModal } = this.state
    this.setState({
      [typeModal]: '',
      isDisplayDropDown: false
    })
  }

  _btnSignUp() {

  }

  _displayDatePicker(type) {
    this.setState({
      pickerKey: type
    }, () => this.picker.onPressDate())
  }

  _datePickerChange(key, value) {
    this.setState({
      [key]: value
    })
  }

  render() {
    const { isDisplayDropDown, dropDownData, pickerKey } = this.state
    return (
      <KeyboardAwareScrollView
        style={styles.keyboardAwareScrollView}
        resetScrollToCoords={{ x: 0, y: 0 }}
      >
        <StatusBar barStyle='light-content' />
        <View style={[styles.rootContainerSignUp]}>
          <SingUpRowTitle
            title={I18n('signUpScreen.loginInfo')}
            marginTop={20}
          />
          <View style={styles.rowFormSignUp}>
            <SignUpTextInput
              placeholder={I18n('signUpScreen.email')}
              placeholderTextColor='#76805D'
              backgroundColor='#E4E2E5'
              type='email'
              onChangeText={(key, value) => this._onChangeText(key, value)}
              value={this.state.email}
              secureTextEntry={false}
              marginTop={10}
            />

            <SignUpTextInput
              placeholder={I18n('signUpScreen.password')}
              placeholderTextColor='#76805D'
              backgroundColor='#E4E2E5'
              type='password'
              onChangeText={(key, value) => this._onChangeText(key, value)}
              value={this.state.password}
              secureTextEntry={true}
              marginTop={10}
            />
          </View>
          <SingUpRowTitle
            title={I18n('signUpScreen.personalInfo')}
            marginTop={40}
          />
          <View style={styles.rowFormSignUp}>
            <SignUpTextInput
              placeholder={I18n('signUpScreen.firstName')}
              placeholderTextColor='#76805D'
              backgroundColor='#E4E2E5'
              type='firstName'
              onChangeText={(key, value) => this._onChangeText(key, value)}
              value={this.state.firstName}
              secureTextEntry={false}
              marginTop={10}
            />
            <SignUpTextInput
              placeholder={I18n('signUpScreen.lastName')}
              placeholderTextColor='#76805D'
              backgroundColor='#E4E2E5'
              type='lastName'
              onChangeText={(key, value) => this._onChangeText(key, value)}
              value={this.state.lastName}
              secureTextEntry={false}
              marginTop={10}
            />
            <SignUpDropDown
              placeholderTextColor='#76805D'
              backgroundColor='#E4E2E5'
              type='sex'
              onPress={(type) => this._showDropDown(type, this.optionsSex)}
              placeholderText={I18n('signUpScreen.sex')}
              value={this.state.sex}
              marginTop={10}
            />
            <SignUpDatePicker
              placeholderTextColor='#76805D'
              backgroundColor='#E4E2E5'
              placeholderText={I18n('signUpScreen.birthday')}
              type='birthday'
              onPress={(type) => this._displayDatePicker(type)}
              value={this.state.birthday}
              marginTop={10}
            />
            <SignUpTextInput
              placeholder={I18n('signUpScreen.identityCard')}
              placeholderTextColor='#76805D'
              backgroundColor='#E4E2E5'
              type='identityCard'
              onChangeText={(key, value) => this._onChangeText(key, value)}
              value={this.state.identityCard}
              secureTextEntry={false}
              marginTop={10}
            />
            <SignUpTextInput
              placeholder={I18n('signUpScreen.phoneNumber')}
              placeholderTextColor='#76805D'
              backgroundColor='#E4E2E5'
              type='phoneNumber'
              onChangeText={(key, value) => this._onChangeText(key, value)}
              value={this.state.phoneNumber}
              secureTextEntry={false}
              marginTop={10}
            />
          </View>
          <View style={styles.rowButton}>
            <MainButton
              text={I18n('signUpScreen.buttonTextSignIUp')}
              backgroundColor='#ED1D25'
              buttonWidth={this.width - 30}
              onPress={() => this._btnSignUp()}
            />
          </View>
          <ModalFilterPicker
            visible={isDisplayDropDown}
            onSelect={(picked) => this._onSelected(picked)}
            onCancel={() => this._onCancel()}
            options={dropDownData}
            cancelButtonText='Cancel'
            showFilter={false}
          />
          <DatePicker
            ref={(picker) => (this.picker = picker)}
            mode='date'
            androidMode='spinner'
            hideText={true}
            format='DD-MM-YYYY'
            minDate='01-01-1800'
            maxDate={moment(new Date().getTime()).locale('en').format('DD-MM-YYYY')}
            confirmBtnText='Confirm'
            cancelBtnText='Cancel'
            showIcon={false}
            onDateChange={(birthday) => {this._datePickerChange(pickerKey, birthday)}}
          />
        </View>
      </KeyboardAwareScrollView>
    )
  }
}

const mapDispatchToProps = {
}

export default connect(null, mapDispatchToProps)(SignUp)