import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet,
  View,
  Dimensions,
  Text
} from 'react-native'

export default class SingUpRowTitle extends Component {

  static propTypes = {
    title: PropTypes.string,
    marginTop: PropTypes.number
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.width = width - 30
  }

  render() {
    const { title, marginTop } = this.props
    return (
      <View style={[styles.titleView, { width: this.width, marginTop }]}>
        <Text style={styles.titleText}>{title}</Text>
      </View>
    )
  }
}

SingUpRowTitle.defaultProps = {
  title: '',
  marginTop: 10
}

const styles = StyleSheet.create({
  titleView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginLeft: 15
  },
  titleText: {
    fontSize: 14,
    color: '#F59220',
    fontWeight: 'bold'
  }
})