import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet,
  Text,
  Dimensions,
  Platform,
  TouchableOpacity
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

export default class SignUpDropDown extends Component {

  static propTypes = {
    type: PropTypes.string,
    backgroundColor: PropTypes.string,
    marginTop: PropTypes.number,
    placeholderTextColor: PropTypes.string,
    placeholderText: PropTypes.string,
    value: PropTypes.string,
    onPress: PropTypes.func
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.inputWidth = width - 30
  }

  render() {
    const {
      type,
      onPress,
      placeholderText,
      marginTop,
      placeholderTextColor,
      backgroundColor,
      value,
      ...props
    } = this.props

    const paddingNumber = Platform.OS === 'ios' ? 7 : 5
    const borderRadius = Platform.OS === 'ios' ? 4 : 4
    const text = value === '' ? placeholderText : value
    const color = value === '' ? placeholderTextColor : '#7F7E80'

    return (
      <TouchableOpacity
        onPress={() => onPress(type)}
        style={
          [styles.viewDropDown, {
            width: this.inputWidth,
            marginTop,
            borderRadius,
            backgroundColor,
            paddingBottom: paddingNumber,
            paddingTop: paddingNumber
          }]
        }
        {...props}
      >
        <Text style={[styles.textDropDown, { color }]}>
          {text}
        </Text>
        <Icon
          style={styles.iconDropDown}
          name='md-arrow-dropdown'
          size={25}
          color='#7F7E80'
        />
      </TouchableOpacity>
    )
  }
}

SignUpDropDown.defaultProps = {
  type: '',
  placeholderText: '',
  backgroundColor: '#E4E2E5',
  value: '',
  marginTop: 0,
  onPress: () => {}
}

const styles = StyleSheet.create({
  viewDropDown: {
    padding: 12,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row'
  },
  textDropDown: {
    color: '#7F7E80'
  },
  iconDropDown: {
    marginTop: 3
  }
})