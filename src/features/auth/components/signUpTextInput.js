import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet,
  TextInput,
  Dimensions,
  Platform
} from 'react-native'

export default class SignUpTextInput extends Component {

  static propTypes = {
    type: PropTypes.string,
    autoCapitalize: PropTypes.string,
    backgroundColor: PropTypes.string,
    marginTop: PropTypes.number,
    onChangeText: PropTypes.func,
    disable: PropTypes.bool
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.inputWidth = width - 30
  }

  render() {
    const {
      autoCapitalize,
      disable,
      onChangeText,
      type,
      marginTop,
      backgroundColor,
      ...props
    } = this.props

    const paddingNumber = Platform.OS === 'ios' ? 12 : 5
    const borderRadius = Platform.OS === 'ios' ? 4 : 4

    return (
      <TextInput
        autoCapitalize={autoCapitalize}
        editable={disable}
        style={[styles.input, {
          width: this.inputWidth,
          marginTop,
          borderRadius,
          paddingBottom: paddingNumber,
          paddingTop: paddingNumber,
          backgroundColor
        }]}
        onChangeText={value => onChangeText(type, value)}
        underlineColorAndroid='transparent'
        {...props}
      />
    )
  }
}

SignUpTextInput.defaultProps = {
  type: '',
  autoCapitalize: 'none',
  marginTop: 0,
  disable: true,
  onChangeText: () => {}
}

const styles = StyleSheet.create({
  input: {
    padding: 12,
    color: '#7F7E80'
  }
})