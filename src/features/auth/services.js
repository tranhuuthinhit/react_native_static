import * as actions from './actions'
import * as ASYNC from '../../utils/asyncStorage'
import I18n from '../../utils/i18n'

function createUser(userInfo) {
  return (dispatch) => {
    dispatch(actions.signUp())
  }
}

function authenticate(userInfo) {
  return (dispatch) => {
    if(userInfo.username === 'test' && userInfo.password === '123') {
      const userProfile = {
        fullname: 'Tran Huu Thinh',
        identityCard: '3412322334',
        satraCard: '9000000437',
        point: 200,
        loginDate: new Date().getTime(),
        birthday: '23 Feb, 1988',
        email: 'thinh.tran@youthdev.net',
        phone: '0979898832',
        sex: 'male'
      }
      ASYNC.setUserInfo(userProfile)
      ASYNC.setLoginStatus('true')
      dispatch(actions.signInSuccess(userProfile))
    } else {
      dispatch(actions.signInFailure(I18n('errorMessage.signInError')))
    }
  }
}

function checkAccount(username) {
  return (dispatch) => {
    if(username === 'test') {
      dispatch(actions.checkAccountSuccess())
    } else {
      dispatch(actions.checkAccountFailure(I18n('errorMessage.checkAccountError')))
    }
  }
}

function activeFromStorage(userProfile) {
  return (dispatch) => {
    dispatch(actions.signInSuccess(userProfile))
  }
}

function logOut() {
  return (dispatch) => {
    ASYNC.setLoginStatus('false')
    dispatch(actions.logOut())
  }
}

function resetError() {
  return (dispatch) => {
    dispatch(actions.resetError())
  }
}

export {
  createUser,
  checkAccount,
  authenticate,
  activeFromStorage,
  resetError,
  logOut
}