import { StyleSheet } from 'react-native'
import * as THEME from '../../constants/mainTheme'

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: 10,
    paddingBottom: 20
  },
  profileVIew: {
    backgroundColor: THEME.PRIMARY_HEADER_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20
  },
  imageAvatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginTop: 20,
    borderColor: '#FFFFFF',
    borderWidth: 3
  },
  cardNumber: {
    color: '#FFFFFF',
    fontSize: 20,
    marginTop: 20,
    fontWeight: 'bold'
  },
  pointView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#FFFFFF',
    borderTopWidth: 1,
    marginTop: 20,
    paddingTop: 20
  },
  point: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  pointNumber: {
    color: '#FFFFFF',
    fontSize: 25,
    fontWeight: 'bold'
  },
  pointText: {
    color: '#FFFFFF',
    fontSize: 14,
    marginTop: 5
  }
})

export default styles