import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Dimensions,
  StatusBar,
  ScrollView
} from 'react-native'
import styles from './styles'
import { connect } from 'react-redux'
import navigationStyles from '../../constants/navigationStyles'
import PromotionCard from '../../components/promotionCard'

export class Promotion extends Component {

  static propTypes = {
    navigator: PropTypes.object
  }

  static navigatorStyle = {
    ...navigationStyles.home
  }

  constructor(props) {
    super(props)
    const { width, height } = Dimensions.get('window')
    this.width = width
    this.height = height
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.rootContainer}>
          <StatusBar barStyle='light-content' />
          <PromotionCard
            title='Vạn quà như ý, đón tết an khang'
            promotionImage='http://demo.satra.com.vn/upload/large/news/satrafoods-daidien.jpg'
            cardWidth={this.width - 40}
          />

          <PromotionCard
            title='Giảm giá 50% sản phẩm dầu thực vật'
            promotionImage='http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg'
            cardWidth={this.width - 40}
          />

          <PromotionCard
            title='Giảm giá 50% sản phẩm dầu thực vật'
            promotionImage='http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg'
            cardWidth={this.width - 40}
          />

          <PromotionCard
            title='Giảm giá 50% sản phẩm dầu thực vật'
            promotionImage='http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg'
            cardWidth={this.width - 40}
          />
        </View>
      </ScrollView>
    )
  }
}

const mapDispatchToProps = {}

export default connect(null, mapDispatchToProps)(Promotion)