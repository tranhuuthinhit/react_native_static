import * as types from './actionTypes'

function initialUserScreen() {
  return {
    type: types.USER_NO_LOGIN
  }
}

function goToGetCodeScreen(screen) {
  return {
    type: types.GO_TO_SCREEN,
    screen
  }
}

function getCode(code) {
  return {
    type: types.GET_CODE,
    code
  }
}

export {
  initialUserScreen,
  goToGetCodeScreen,
  getCode
}