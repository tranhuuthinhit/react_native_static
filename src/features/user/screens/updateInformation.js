import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  StatusBar,
  ScrollView,
  View
} from 'react-native'
import { connect } from 'react-redux'
import styles from './styles'
import navigationStyles from '../../../constants/navigationStyles'
import * as ROUTES from '../../../constants/routes'
import ProfileInputRow from '../../../components/profileRowInput'
import ProfileTextRowGender from '../../../components/profileRowGender'
import CheckBox from '../../../components/checkBox'
import MainButton from '../../../components/mainButton'
import I18n from '../../../utils/i18n'

export class UpdateInformation extends Component {

  static propTypes = {
    navigator: PropTypes.object,
    fullname: PropTypes.string,
    identityCard: PropTypes.string,
    birthday: PropTypes.string,
    email: PropTypes.string,
    phone: PropTypes.string,
    sex: PropTypes.string
  }

  static navigatorStyle = {
    ...navigationStyles.myInformation
  }

  constructor(props) {
    super(props)
    const { width, height } = Dimensions.get('window')
    this.width = width
    this.mainWidth = width - 40
    this.height = height
    this.state = {
      allowPhone: false,
      allowHome: false,
      allowEmail: false,
      genderValue: false,
      fullname: '',
      identityCard: '',
      birthday: '',
      email: '',
      phone: '',
      sex: ''
    }
  }

  componentWillMount() {
    const {
      fullname,
      identityCard,
      birthday,
      email,
      phone,
      sex
    } = this.props
    this.setState({
      fullname,
      identityCard,
      birthday,
      email,
      phone,
      sex
    })
  }

  _onNavigatorEvent(event) {
    if(event.type === 'NavBarButtonPress') {
      if(event.id === 'create') {
        this._goToOtherScreen(ROUTES.MY_INFORMATION, 'ACCOUNT')
      }
    }
  }

  _goToOtherScreen(screen, title) {
    this.props.navigator.push({
      screen,
      title,
      backButtonTitle: ''
    })
  }

  _handleUpdateState(key, value) {
    this.setState({
      [key]: !value
    })
  }

  render() {
    const {
      allowPhone,
      allowHome,
      allowEmail,
      genderValue,
      fullname,
      identityCard,
      birthday,
      email,
      phone,
      sex
    } = this.state
    return (
      <ScrollView style={styles.scrollView}>
        <View style={styles.rootContainer}>
          <StatusBar barStyle='light-content' />
          <ProfileInputRow
            iconName='md-person'
            text={fullname}
          />
          <ProfileInputRow
            iconName='md-card'
            text={identityCard}
          />
          <ProfileInputRow
            iconName='md-calendar'
            text={birthday}
          />
          <ProfileInputRow
            iconName='md-mail'
            text={email}
          />
          <ProfileInputRow
            iconName='md-call'
            text={phone}
          />
          <ProfileTextRowGender
            rowIcon='ios-people'
            isEnableTouch={true}
            callback={() => this._handleUpdateState('genderValue', genderValue)}
            sex={sex}
          />
          <View style={[styles.termView, { width: this.width - 40 }]}>
            <CheckBox
              iconName={allowPhone ? 'md-checkbox' : 'md-square-outline'}
              label={I18n('myInformationScreen.allow1')}
              value={allowPhone}
              onPress={() => this._handleUpdateState('allowPhone', allowPhone)}
            />

            <CheckBox
              iconName={allowHome ? 'md-checkbox' : 'md-square-outline'}
              label={I18n('myInformationScreen.allow2')}
              value={allowHome}
              onPress={() => this._handleUpdateState('allowHome', allowHome)}
            />

            <CheckBox
              iconName={allowEmail ? 'md-checkbox' : 'md-square-outline'}
              label={I18n('myInformationScreen.allow3')}
              value={allowEmail}
              onPress={() => this._handleUpdateState('allowEmail', allowEmail)}
            />
          </View>

          <View style={styles.buttonView}>
            <MainButton
              text={I18n('updateInformation.button.update')}
              buttonWidth={this.mainWidth}
            />
          </View>

        </View>
      </ScrollView>
    )
  }
}

const mapDispatchToProps = {}

const mapStateToProps = (state) => ({
  fullname: state.auth.userProfile.fullname,
  identityCard: state.auth.userProfile.identityCard,
  birthday: state.auth.userProfile.birthday,
  email: state.auth.userProfile.email,
  phone: state.auth.userProfile.phone,
  sex: state.auth.userProfile.sex
})

export default connect(mapStateToProps, mapDispatchToProps)(UpdateInformation)