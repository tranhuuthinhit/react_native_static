import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  Alert
} from 'react-native'
import { connect } from 'react-redux'
import navigationStyles from '../../../constants/navigationStyles'
import Icon from 'react-native-vector-icons/Ionicons'
import I18n from '../../../utils/i18n'
import * as ROUTES from '../../../constants/routes'
import SignIn from '../../auth/screens/signIn'
import MyProfile from './myProfile'
import { logOut } from '../../auth/services'

export class User extends Component {

  static propTypes = {
    navigator: PropTypes.object,
    dispatchLogOut: PropTypes.func,
    isAuthencated: PropTypes.bool
  }

  static navigatorStyle = {
    ...navigationStyles.myInformation
  }

  constructor(props) {
    super(props)
    const { width, height } = Dimensions.get('window')
    this.width = width
    this.height = height
    this.mainWidth = width - 40
    this.props.navigator.setOnNavigatorEvent(this._onNavigatorEvent.bind(this))
  }

  async prepareIcons() {
    const icons = await Promise.all([
      Icon.getImageSource('md-person', 25),
      Icon.getImageSource('md-settings', 25),
      Icon.getImageSource('ios-paper', 25),
      Icon.getImageSource('md-log-out', 25)
    ])
    const [person, setting, history, logout] = icons
    return { person, setting, history, logout }
  }

  _onNavigatorEvent(event) {
    if(event.type === 'NavBarButtonPress') {
      if(event.id === 'information') {
        this._goToOtherScreen(ROUTES.MY_INFORMATION, I18n('myInformationScreen.title'))
      } else if(event.id === 'setting') {
        this._goToOtherScreen(ROUTES.SETTING, I18n('settingScreen.title'))
      } else if(event.id === 'history') {
        this._goToOtherScreen(ROUTES.MY_HISTORY, I18n('historyScreen.title'))
      } else if(event.id === 'logout') {
        this._logOutPress()
      }
    }
  }

  async _setNavBarButtons() {

    const { isAuthencated } = this.props

    if(isAuthencated) {

      const icons = await this.prepareIcons()
      const rightButtons = [
        {
          title: 'Logout',
          icon: icons.logout,
          id: 'logout'
        },
        {
          title: 'My Information',
          icon: icons.person,
          id: 'information'
        }
      ]

      const leftButtons = [
        {
          title: 'History',
          icon: icons.history,
          id: 'history'
        }
      ]

      this.props.navigator.setButtons({
        rightButtons,
        leftButtons,
        animated: true
      })

      this.props.navigator.setTitle({
        title: I18n('profileScreen.title')
      })
    } else {
      this.props.navigator.setButtons({
        rightButtons: [],
        leftButtons: [],
        animated: true
      })

      this.props.navigator.setTitle({
        title: I18n('signInScreen.title')
      })
    }
  }

  _logOutPress() {
    Alert.alert(
      I18n('errorTitle'),
      I18n('errorMessage.confirmLogout'),
      [
        {
          text: I18n('alertButton.Cancel')
        },
        {
          text: I18n('alertButton.OK'), onPress: () => this.props.dispatchLogOut()
        }
      ],
      { cancelable: false }
    )
  }

  _goToOtherScreen(screen, title) {
    this.props.navigator.push({
      screen,
      title,
      backButtonTitle: ''
    })
  }

  render() {
    const { isAuthencated } = this.props
    this._setNavBarButtons()

    if(!isAuthencated) {
      return (<SignIn {...this.props} />)
    }
    return (<MyProfile {...this.props} />)
  }
}

const mapDispatchToProps = {
  dispatchLogOut: () => logOut()
}

const mapStateToProps = (state) => ({
  isAuthencated: state.auth.isAuthencated
})

export default connect(mapStateToProps, mapDispatchToProps)(User)