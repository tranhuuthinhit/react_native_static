import { StyleSheet } from 'react-native'
import * as THEME from '../../../constants/mainTheme'

const styles = StyleSheet.create({
  scrollView: {
    flex: 1,
    backgroundColor: '#FFFFFF'
  },
  rootContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#FFFFFF',
    paddingBottom: 20
  },
  viewCardInfo: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50
  },
  rowCardInfo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10
  },
  leftText: {
    fontSize: 14,
    color: '#949598',
    marginTop: 5
  },
  rightText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#8CC63F'
  },
  rowButton: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20
  },
  myBarCode: {
    marginTop: 20
  },
  myCodeText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#8CC63F',
    marginTop: 20
  },
  profileVIew: {
    backgroundColor: THEME.PRIMARY_HEADER_COLOR,
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 20
  },
  imageAvatar: {
    width: 100,
    height: 100,
    borderRadius: 50,
    marginTop: 20,
    borderColor: '#FFFFFF',
    borderWidth: 3
  },
  cardNumber: {
    color: '#FFFFFF',
    fontSize: 25,
    marginTop: 20,
    fontWeight: 'bold'
  },
  pointView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    borderColor: '#FFFFFF',
    borderTopWidth: 1,
    marginTop: 20,
    paddingTop: 20
  },
  point: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  pointNumber: {
    color: '#FFFFFF',
    fontSize: 25
  },
  pointText: {
    color: '#FFFFFF',
    fontSize: 14,
    marginTop: 5
  },
  upLine: {
    width: 1,
    height: 50,
    backgroundColor: '#FFFFFF'
  },
  barCodeView: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column',
    marginTop: 30
  },
  noteScanTextView: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20
  },
  noteText: {
    fontSize: 14,
    textAlign: 'center',
    color: '#F59220'
  },
  codeNumber: {
    fontSize: 14,
    textAlign: 'center',
    color: '#7F7E80'
  },
  termView: {
    backgroundColor: '#FFE1D0',
    borderRadius: 5,
    padding: 20,
    paddingBottom: 10,
    paddingRight: 25,
    marginTop: 20,
    flexDirection: 'column'
  },
  buttonView: {
    marginTop: 20,
    justifyContent: 'center',
    alignItems: 'center'
  },
  rowForm: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 50
  }
})

export default styles