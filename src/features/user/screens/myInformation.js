import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  StatusBar,
  ScrollView,
  View
} from 'react-native'
import { connect } from 'react-redux'
import styles from './styles'
import navigationStyles from '../../../constants/navigationStyles'
import Icon from 'react-native-vector-icons/Ionicons'
import * as ROUTES from '../../../constants/routes'
import ProfileTextRow from '../../../components/profileRow'
import ProfileTextRowGender from '../../../components/profileRowGender'
import ProfileTextRowAction from '../../../components/profileRowAction'
import CheckBox from '../../../components/checkBox'
import I18n from '../../../utils/i18n'

export class MyInformation extends Component {

  static propTypes = {
    navigator: PropTypes.object,
    fullname: PropTypes.string,
    identityCard: PropTypes.string,
    birthday: PropTypes.string,
    email: PropTypes.string,
    phone: PropTypes.string,
    sex: PropTypes.string
  }

  static navigatorStyle = {
    ...navigationStyles.myInformation
  }

  constructor(props) {
    super(props)
    const { width, height } = Dimensions.get('window')
    this.width = width
    this.mainWidth = width - 40
    this.height = height
    this.state = {
      allowPhone: false,
      allowHome: false,
      allowEmail: false
    }
    this._setNavBarButtons()
    this.props.navigator.setOnNavigatorEvent(this._onNavigatorEvent.bind(this))
  }

  _onNavigatorEvent(event) {
    if(event.type === 'NavBarButtonPress') {
      if(event.id === 'create') {
        this._goToOtherScreen(ROUTES.UPDATE_INFORMATION, I18n('updateInformation.title'))
      }
    }
  }

  _goToOtherScreen(screen, title) {
    this.props.navigator.push({
      screen,
      title,
      backButtonTitle: ''
    })
  }

  async prepareIcons() {
    const icons = await Promise.all([
      Icon.getImageSource('md-create', 23)
    ])
    const [create] = icons
    return { create }
  }

  async _setNavBarButtons() {
    const icons = await this.prepareIcons()
    this.props.navigator.setButtons({
      rightButtons: [
        {
          title: 'Update Profile',
          icon: icons.create,
          id: 'create'
        }
      ]
    })
  }

  render() {
    const {
      fullname,
      identityCard,
      birthday,
      email,
      phone,
      sex
    } = this.props

    const {
      allowPhone,
      allowHome,
      allowEmail
    } = this.state

    return (
      <ScrollView style={styles.scrollView}>
        <View style={styles.rootContainer}>
          <StatusBar barStyle='light-content' />
          <ProfileTextRowAction
            iconName='md-refresh'
            onPress={() => this._goToOtherScreen(ROUTES.CHANGE_PASSWORD, I18n('changePasswordScreen.title'))}
            text={I18n('myInformationScreen.changePassword')}
          />
          <ProfileTextRow
            iconName='md-person'
            text={fullname}
          />
          <ProfileTextRow
            iconName='md-card'
            text={identityCard}
          />
          <ProfileTextRow
            iconName='md-calendar'
            text={birthday}
          />
          <ProfileTextRow
            iconName='md-mail'
            text={email}
          />
          <ProfileTextRow
            iconName='md-call'
            text={phone}
          />
          <ProfileTextRowGender
            rowIcon='ios-people'
            sex={sex}
          />
          <View style={[styles.termView, { width: this.mainWidth }]}>
            <CheckBox
              iconName={allowPhone ? 'md-checkbox' : 'md-square-outline'}
              label={I18n('myInformationScreen.allow1')}
              value={allowPhone}
              disabled={true}
            />

            <CheckBox
              iconName={allowHome ? 'md-checkbox' : 'md-square-outline'}
              label={I18n('myInformationScreen.allow2')}
              value={allowHome}
              disabled={true}
            />

            <CheckBox
              iconName={allowEmail ? 'md-checkbox' : 'md-square-outline'}
              label={I18n('myInformationScreen.allow3')}
              value={allowEmail}
              disabled={true}
            />
          </View>

        </View>
      </ScrollView>
    )
  }
}

const mapStateToProps = (state) => ({
  fullname: state.auth.userProfile.fullname,
  identityCard: state.auth.userProfile.identityCard,
  birthday: state.auth.userProfile.birthday,
  email: state.auth.userProfile.email,
  phone: state.auth.userProfile.phone,
  sex: state.auth.userProfile.sex
})

export default connect(mapStateToProps, null)(MyInformation)