import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  View,
  StatusBar
} from 'react-native'
import styles from './styles'
import { connect } from 'react-redux'
import navigationStyles from '../../../constants/navigationStyles'
import MainTextInput from '../../../components/mainTextInput'
import LogoImage from '../../../components/logoImage'
import CardBonus from '../../../components/cardBonus'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import I18n from '../../../utils/i18n'
import MainButton from '../../../components/mainButton'

export class ChangePassword extends Component {

  static propTypes = {
  }

  static navigatorStyle = {
    ...navigationStyles.signIn
  }

  constructor(props) {
    super(props)
    const { width, height } = Dimensions.get('window')
    this.width = width
    this.height = height
    this.state = {
    }
  }

  _onChangeText(key, value) {
    this.setState({
      [key]: value
    })
  }

  render() {
    return (
      <KeyboardAwareScrollView
        style={styles.keyboardAwareScrollView}
      >
        <StatusBar barStyle='light-content' />
        <View style={[styles.rootContainer]}>
          <LogoImage marginTop={40} />
          <CardBonus marginTop={30} />
          <View style={[styles.rowForm]}>
            <MainTextInput
              placeholder={I18n('signInScreen.placeholderEmail')}
              placeholderTextColor='#D2D6D3'
              type='username'
              onChangeText={(key, value) => this._onChangeText(key, value)}
              value={this.state.username}
            />
          </View>
          <View style={styles.rowButton}>
            <MainButton
              text={I18n('signInScreen.buttonText')}
              backgroundColor='#ED1D25'
              buttonWidth={this.width - 100}
              onPress={() => this._signIn()}
            />
          </View>
        </View>
      </KeyboardAwareScrollView>
    )
  }
}

const mapDispatchToProps = {
}

const mapStateToProps = (state) => ({
})

export default connect(mapStateToProps, mapDispatchToProps)(ChangePassword)