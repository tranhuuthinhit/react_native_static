import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Text,
  Dimensions,
  Image,
  StatusBar,
  ScrollView
} from 'react-native'
import styles from './styles'
import { connect } from 'react-redux'
import navigationStyles from '../../../constants/navigationStyles'
import Barcode from 'react-native-barcode-builder'
import I18n from '../../../utils/i18n'
import moment from 'moment'
import IMAGES from '../../../assets/images'

export class MyProfile extends Component {

  static propTypes = {
    isAuthencated: PropTypes.bool,
    fullname: PropTypes.string,
    satraCard: PropTypes.string,
    loginDate: PropTypes.number,
    point: PropTypes.number
  }

  static navigatorStyle = {
    ...navigationStyles.myInformation
  }

  constructor(props) {
    super(props)
    const { width, height } = Dimensions.get('window')
    this.width = width
    this.height = height
  }

  render() {
    const {
      isAuthencated,
      fullname,
      satraCard,
      loginDate,
      point
    } = this.props

    if(isAuthencated) {
      return (
        <ScrollView style={styles.scrollView}>
          <View style={styles.rootContainer}>
            <StatusBar barStyle='light-content' />
            <View style={[styles.profileVIew, { width: this.width }]}>
              <Image
                source={IMAGES.noAvatar}
                style={styles.imageAvatar}
              />
              <Text style={styles.cardNumber}>{fullname}</Text>
              <View style={[styles.pointView, { width: this.width - 30 }]}>
                <View style={[styles.point, { width: (this.width - 30)/2 }]}>
                  <Text style={styles.pointNumber}>{point}</Text>
                  <Text style={styles.pointText}>{I18n('profileScreen.pointText')}</Text>
                </View>
                <View style={styles.upLine} />
                <View style={[styles.point, { width: (this.width - 30)/2 }]}>
                  <Text style={styles.pointNumber}>{moment(loginDate).locale('en').format('DD/MM/YYYY')}</Text>
                  <Text style={styles.pointText}>{I18n('profileScreen.asOf')}</Text>
                </View>
              </View>
            </View>
            <View style={styles.barCodeView}>
              <Barcode
                value={satraCard}
                format='CODE128'
                width={3}
              />
              <Text style={styles.codeNumber}>{satraCard}</Text>
              <View style={[styles.noteScanTextView, { width: this.width - 100 }]}>
                <Text style={styles.noteText}>{I18n('profileScreen.noteScanText')}</Text>
              </View>
            </View>
          </View>
        </ScrollView>
      )
    }
    return null
  }
}

const mapStateToProps = (state) => ({
  isAuthencated: state.auth.isAuthencated,
  fullname: state.auth.userProfile.fullname,
  satraCard: state.auth.userProfile.satraCard,
  loginDate: state.auth.userProfile.loginDate,
  point: state.auth.userProfile.point
})

export default connect(mapStateToProps, null)(MyProfile)