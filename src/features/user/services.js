import {
  AsyncStorage
} from 'react-native'
import * as mainActions from '../../app/actions'
import * as actions from './actions'

function initialUserScreen() {
  return (dispatch) => {
    dispatch(actions.initialUserScreen())
  }
}

function getCode() {
  return (dispatch) => {
    const code = '9000000437'
    dispatch(actions.getCode(code))
  }
}

function checkAuth() {
  return dispatch => {
    dispatch(actions.initialUserScreen())
    let screen = 'signIn'
    setTimeout(() => {
      AsyncStorage.getItem('PROFILE')
        .then(values => {
          if (values != null && values !== undefined) {
            screen = 'home'
            dispatch(mainActions.changeAppRoot(screen))
          } else {
            dispatch(mainActions.changeAppRoot(screen))
          }
        })
    }, 2000)
  }
}

export {
  initialUserScreen,
  checkAuth,
  getCode
}