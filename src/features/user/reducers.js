import * as types from './actionTypes'
import Immutable from 'seamless-immutable'

const initialState = Immutable({
  isLoadingCode: true
})

export default function user(state = initialState, action = {}) {
  switch(action.type) {
    case types.USER_NO_LOGIN:
      return {
        ...state
      }
    case types.GET_CODE:
      return {
        ...state,
        isLoadingCode: false,
        myCode: action.code
      }
    default:
      return state
  }
}