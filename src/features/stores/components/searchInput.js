import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  TextInput,
  View,
  StyleSheet,
  TouchableOpacity
} from 'react-native'
import Icon from 'react-native-vector-icons/Ionicons'

export default class SearchInput extends Component {

  static propTypes = {
    value: PropTypes.string,
    width: PropTypes.number,
    onPress: PropTypes.func
  }

  render() {
    const { value, onPress, width } = this.props
    return (
      <View style={[styles.searchInputView, { width: width - 20 }]}>
        <Icon style={styles.leftIcon} name='md-search' size={20} color='#7F7E80' />
        <TextInput
          style={[styles.input, { width: width - 90 }]}
          underlineColorAndroid='transparent'
          value={value}
        />
        <TouchableOpacity
          onPress={() => onPress()}
        >
          <Icon style={styles.rightIcon} name='md-close-circle' size={20} color='#7F7E80' />
        </TouchableOpacity>
      </View>
    )
  }
}

SearchInput.defaultProps = {
  value: '',
  width: 0,
  onPress: () => {}
}

const styles = StyleSheet.create({
  searchInputView: {
    height: 35,
    backgroundColor: '#FFFFFF',
    borderRadius: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start'
  },
  input: {
    height: 33,
    marginLeft: 5,
    color: '#7F7E80'
  },
  leftIcon: {
    marginLeft: 7,
    padding: 3
  },
  rightIcon: {
    marginLeft: 5,
    padding: 3,
    marginTop: 2
  }
})