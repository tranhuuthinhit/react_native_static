import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Dimensions,
  StatusBar,
  ScrollView
} from 'react-native'
import styles from './styles'
import { connect } from 'react-redux'
import navigationStyles from '../../../constants/navigationStyles'
import StoreCard from '../../../components/storeCard'
import SearchInput from '../components/searchInput'

export class AllStores extends Component {

  static propTypes = {
    navigator: PropTypes.object
  }

  static navigatorStyle = {
    ...navigationStyles.home
  }

  constructor(props) {
    super(props)
    const { width, height } = Dimensions.get('window')
    this.width = width
    this.height = height
  }

  render() {
    return (
      <View>
        <StatusBar barStyle='light-content' />
        <View style={styles.allStoreSearchView}>
          <SearchInput
            value=''
            width={this.width}
          />
        </View>
        <ScrollView style={styles.allStoreScrollView}>
          <View style={styles.allStoreContainer}>
            <StoreCard
              storeName='SATRA FOODS PHÚ THUẬN'
              storeAddress='35 Phú Thuận, P.Phú Thuận, Q7, Tp.HCM'
              storeImage='http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg'
              cardWidth={this.width - 40}
              isIconDisplay={true}
              iconName='directions'
              iconSize={25}
              iconColor='#FFFFFF'
              backgroundColor='#F59220'
            />

            <StoreCard
              storeName='SATRA FOODS PHÚ THUẬN'
              storeAddress='35 Phú Thuận, P.Phú Thuận, Q7, Tp.HCM'
              storeImage='http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg'
              cardWidth={this.width - 40}
              isIconDisplay={true}
              iconName='directions'
              iconSize={25}
              iconColor='#FFFFFF'
              backgroundColor='#F59220'
            />

            <StoreCard
              storeName='SATRA FOODS PHÚ THUẬN'
              storeAddress='35 Phú Thuận, P.Phú Thuận, Q7, Tp.HCM'
              storeImage='http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg'
              cardWidth={this.width - 40}
              isIconDisplay={true}
              iconName='directions'
              iconSize={25}
              iconColor='#FFFFFF'
              backgroundColor='#F59220'
            />

            <StoreCard
              storeName='SATRA FOODS PHÚ THUẬN'
              storeAddress='35 Phú Thuận, P.Phú Thuận, Q7, Tp.HCM'
              storeImage='http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg'
              cardWidth={this.width - 40}
              isIconDisplay={true}
              iconName='directions'
              iconSize={25}
              iconColor='#FFFFFF'
              backgroundColor='#F59220'
            />

            <StoreCard
              storeName='SATRA FOODS PHÚ THUẬN'
              storeAddress='35 Phú Thuận, P.Phú Thuận, Q7, Tp.HCM'
              storeImage='http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg'
              cardWidth={this.width - 40}
              isIconDisplay={true}
              iconName='directions'
              iconSize={25}
              iconColor='#FFFFFF'
              backgroundColor='#F59220'
            />

            <StoreCard
              storeName='SATRA FOODS PHÚ THUẬN'
              storeAddress='35 Phú Thuận, P.Phú Thuận, Q7, Tp.HCM'
              storeImage='http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg'
              cardWidth={this.width - 40}
              isIconDisplay={true}
              iconName='directions'
              iconSize={25}
              iconColor='#FFFFFF'
              backgroundColor='#F59220'
            />

            <StoreCard
              storeName='SATRA FOODS PHÚ THUẬN'
              storeAddress='35 Phú Thuận, P.Phú Thuận, Q7, Tp.HCM'
              storeImage='http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg'
              cardWidth={this.width - 40}
              isIconDisplay={true}
              iconName='directions'
              iconSize={25}
              iconColor='#FFFFFF'
              backgroundColor='#F59220'
            />

            <StoreCard
              storeName='SATRA FOODS PHÚ THUẬN'
              storeAddress='35 Phú Thuận, P.Phú Thuận, Q7, Tp.HCM'
              storeImage='http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg'
              cardWidth={this.width - 40}
              isIconDisplay={true}
              iconName='directions'
              iconSize={25}
              iconColor='#FFFFFF'
              backgroundColor='#F59220'
            />

            <StoreCard
              storeName='SATRA FOODS PHÚ THUẬN'
              storeAddress='35 Phú Thuận, P.Phú Thuận, Q7, Tp.HCM'
              storeImage='http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg'
              cardWidth={this.width - 40}
              isIconDisplay={true}
              iconName='directions'
              iconSize={25}
              iconColor='#FFFFFF'
              backgroundColor='#F59220'
            />

            <StoreCard
              storeName='SATRA FOODS PHÚ THUẬN'
              storeAddress='35 Phú Thuận, P.Phú Thuận, Q7, Tp.HCM'
              storeImage='http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg'
              cardWidth={this.width - 40}
              isIconDisplay={true}
              iconName='directions'
              iconSize={25}
              iconColor='#FFFFFF'
              backgroundColor='#F59220'
            />


          </View>
        </ScrollView>
      </View>
    )
  }
}

const mapDispatchToProps = {}

export default connect(null, mapDispatchToProps)(AllStores)