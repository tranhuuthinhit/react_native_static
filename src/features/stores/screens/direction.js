import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Dimensions,
  Text,
  Image
} from 'react-native'
import { connect } from 'react-redux'
import { getStores } from '../services'
import navigationStyles from '../../../constants/navigationStyles'
import styles from './styles'
import MapView, { Marker } from 'react-native-maps'
import MapViewDirections from 'react-native-maps-directions'

export class StoresDirection extends Component {

  static propTypes = {
    dispatchGetStores: PropTypes.func
  }

  static navigatorStyle = {
    ...navigationStyles.store
  }

  constructor(props) {
    super(props)
    const { width, height } = Dimensions.get('window')
    this.width = width
    this.height = height

    this.ASPECT_RATIO = width / height
    this.LATITUDE = 10.8059374
    this.LONGITUDE = 106.7163613
    this.LATITUDE_DELTA = 0.0922
    this.LONGITUDE_DELTA = this.LATITUDE_DELTA * this.ASPECT_RATIO

    this.coordinates = [
      {
        latitude: 10.8059374,
        longitude: 106.7163613
      },
      {
        latitude: 10.827167,
        longitude: 106.719258
      }
    ]
    this.initialRegion = {
      latitude: this.LATITUDE,
      longitude: this.LONGITUDE,
      latitudeDelta: this.LATITUDE_DELTA,
      longitudeDelta: this.LONGITUDE_DELTA
    }
    this.GOOGLE_MAPS_APIKEY = 'AIzaSyA6r31Xdtfy-YIPON83frsVCSffU8FdGZg'
  }

  componentWillMount() {
    this.props.dispatchGetStores()
  }

  render() {
    return (
      <View style={styles.rootContainer}>
        <MapView initialRegion={this.initialRegion} style={styles.mapView}>
          <Marker coordinate={this.coordinates[0]} />
          <Marker coordinate={this.coordinates[1]} />
          <MapViewDirections
            origin={this.coordinates[0]}
            destination={this.coordinates[1]}
            apikey={this.GOOGLE_MAPS_APIKEY}
            strokeWidth={3}
            strokeColor='hotpink'
          />
        </MapView>
        <View style={{width: this.width, backgroundColor: '#FFFFFF', height: 90, position: 'absolute', bottom: 0, flexDirection: 'column'}}>
          <View style={{flexDirection: 'row', marginTop: 20, width: this.width - 40, marginLeft: 20, justifyContent: 'space-between'}}>
            <View>
              <Text style={{fontSize: 16, fontWeight: 'bold'}}>{'Satra Foods Pham Van Dong'.toUpperCase()}</Text>
              <Text style={{fontSize: 12, marginTop: 5, color: '#7F7E80', flexWrap: 'wrap'}}>123 Pham Van Dong, P.2, Q.Go Vap, Tp.HCM</Text>
            </View>
            <View>
              <Image source={{uri: 'http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg'}} style={{width: 50, height: 50, borderRadius: 3, resizeMode: 'cover'}} />
            </View>
          </View>
        </View>
      </View>
    )
  }
}

const mapDispatchToProps = {
  dispatchGetStores: () => getStores()
}

const mapStateToProps = (state) => ({
  stores: state.stores
})

export default connect(mapStateToProps, mapDispatchToProps)(StoresDirection)