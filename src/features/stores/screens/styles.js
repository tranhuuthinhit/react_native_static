import { StyleSheet, Dimensions } from 'react-native'
const { width } = Dimensions.get('window')


const styles = StyleSheet.create({
  rootContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  mapView: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  allStoreContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingBottom: 20,
    backgroundColor: '#FFFFFF'
  },
  viewButton: {
    flexDirection: 'row',
    width,
    marginTop: 20,
    paddingLeft: 20,
    paddingRight: 20,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  nearbyStoreView: {
    width,
    backgroundColor: '#FFFFFF',
    height: 150,
    position: 'absolute',
    bottom: 0,
    flexDirection: 'column'
  },
  storeInfo: {
    flexDirection: 'row',
    marginTop: 20,
    width: width - 40,
    marginLeft: 20,
    justifyContent: 'space-between'
  },
  storeNameText: {
    fontSize: 16,
    fontWeight: 'bold'
  },
  storeAddressText: {
    fontSize: 12,
    marginTop: 5,
    color: '#7F7E80',
    flexWrap: 'wrap'
  },
  storeImage: {
    width: 50,
    height: 50,
    borderRadius: 3,
    resizeMode: 'cover'
  },
  allStoreScrollView: {
    marginBottom: 50,
    paddingTop: 10
  },
  allStoreSearchView: {
    width,
    height: 50,
    backgroundColor: '#E0DFE1',
    justifyContent: 'center',
    alignItems: 'center'
  }
})

export default styles