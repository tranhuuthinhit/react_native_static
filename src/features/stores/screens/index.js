import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Dimensions,
  Text,
  Image
} from 'react-native'
import { connect } from 'react-redux'
import { loadNearbyStore, updateCurrentPosition } from '../services'
import navigationStyles from '../../../constants/navigationStyles'
import styles from './styles'
import MapView, { Marker } from 'react-native-maps'
import MainButton from '../../../components/mainButton'
import I18n from '../../../utils/i18n'
import * as ROUTES from '../../../constants/routes'
import IMAGES from '../../../assets/images'

export class Stores extends Component {

  static propTypes = {
    navigator: PropTypes.object,
    dispatchLoadNearbyStore: PropTypes.func,
    dispatchUpdateCurrentPosition: PropTypes.func,
    nearbyStore: PropTypes.object,
    isLoadingNearbyStore: PropTypes.bool,
    isLoading: PropTypes.bool,
    initialRegion: PropTypes.object
  }

  static navigatorStyle = {
    ...navigationStyles.store
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.buttonWidth = (width/2) - 30

    this.GOOGLE_MAPS_APIKEY = 'AIzaSyA6r31Xdtfy-YIPON83frsVCSffU8FdGZg'
  }

  componentWillMount() {
    this.props.dispatchLoadNearbyStore()
    this.props.dispatchUpdateCurrentPosition()
  }

  _goToOtherScreen(screen, title) {
    this.props.navigator.push({
      screen,
      title,
      backButtonTitle: ''
    })
  }

  render() {

    const {
      nearbyStore,
      isLoading,
      isLoadingNearbyStore,
      initialRegion
    } = this.props

    if(!isLoading && !isLoadingNearbyStore) {
      return (
        <View style={styles.rootContainer}>
          <MapView
            initialRegion={initialRegion}
            style={styles.mapView}
            loadingEnabled={true}
            showsUserLocation={true}
            loadingIndicatorColor='#FFFFFF'
            loadingBackgroundColor='#CCCCCC'
          >
            <Marker
              coordinate={nearbyStore.location}
              title={nearbyStore.name}
              image={IMAGES.logoMap}
            />

          </MapView>
          <View style={styles.nearbyStoreView}>
            <View style={styles.storeInfo}>
              <View>
                <Text style={styles.storeNameText}>{nearbyStore.name.toUpperCase()}</Text>
                <Text style={styles.storeAddressText}>{nearbyStore.address}</Text>
              </View>
              <View>
                <Image
                  source={{uri: nearbyStore.image}}
                  style={styles.storeImage}
                />
              </View>
            </View>
            <View style={styles.viewButton}>
              <MainButton
                text={I18n('nearbyStore.button.direction')}
                backgroundColor='#ED1D25'
                buttonWidth={this.buttonWidth}
                onPress={() => this._goToOtherScreen(ROUTES.STORE_DIRECTION, I18n('storeDirectionScreen.title'))}
              />

              <MainButton
                text={I18n('nearbyStore.button.search')}
                backgroundColor='#8BC540'
                buttonWidth={this.buttonWidth}
                onPress={() => this._goToOtherScreen(ROUTES.ALL_STORES, I18n('storeScreen.title'))}
              />
            </View>
          </View>
        </View>
      )
    }
    return null
  }
}

const mapDispatchToProps = {
  dispatchLoadNearbyStore: () => loadNearbyStore(),
  dispatchUpdateCurrentPosition: () => updateCurrentPosition()
}

const mapStateToProps = (state) => ({
  nearbyStore: state.stores.nearbyStore,
  isLoadingNearbyStore: state.stores.isLoadingNearbyStore,
  isLoading: state.stores.isLoading,
  initialRegion: state.stores.initialRegion
})

export default connect(mapStateToProps, mapDispatchToProps)(Stores)