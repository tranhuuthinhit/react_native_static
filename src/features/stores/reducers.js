import * as types from './actionTypes'
import Immutable from 'seamless-immutable'

const initialState = Immutable({
  isLoading: true,
  currentPosition: {},
  initialRegion: {},
  nearbyStore: {},
  stores: [],
  isLoadingNearbyStore: true
})

export default function stores(state = initialState, action = {}) {
  switch(action.type) {
    case types.GET_STORES:
      return {
        ...state,
        stores: action.stores
      }
    case types.LOADIND_NEARBY_STORE:
      return {
        ...state,
        isLoadingNearbyStore: true
      }
    case types.LOADED_NEARBY_STORE:
      return {
        ...state,
        isLoadingNearbyStore: false,
        nearbyStore: action.store
      }
    case types.UPDATE_CURRENT_POSITION:
      return {
        ...state,
        isLoading: false,
        initialRegion: action.initialRegion,
        currentPosition: action.currentPosition
      }
    default:
      return state
  }
}