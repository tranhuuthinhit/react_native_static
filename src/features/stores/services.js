import {
  Dimensions
} from 'react-native'
import Geolocation from 'react-native-geolocation-service'
import * as actions from './actions'

const { width, height } = Dimensions.get('window')
const ASPECT_RATIO = (width / (height - 150))

function getStores() {
  return dispatch => {
    dispatch(actions.getStores())
  }
}

function loadNearbyStore() {
  return dispatch => {
    dispatch(actions.loadingNearbyStore())
    const store = {
      name: 'Satra Foods Pham Van Dong',
      address: '123 Pham Van Dong, P.2, Q.Go Vap, Tp.HCM',
      image: 'http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg',
      location: {
        latitude: 10.8059374,
        longitude: 106.7163613
      }
    }

    setTimeout(() => {
      dispatch(actions.loadedNearbyStore(store))
    }, 2000)
  }
}

function updateCurrentPosition(position) {
  return dispatch => {
    Geolocation.getCurrentPosition(
      (position) => {
        const initialRegion = {
          latitude:       position.coords.latitude,
          longitude:      position.coords.longitude,
          latitudeDelta:  0.0922,
          longitudeDelta: (0.0922 * ASPECT_RATIO)
        }

        const currentPosition = {
          latitude:       position.coords.latitude,
          longitude:      position.coords.longitude
        }
        dispatch(actions.updateCurrentPosition(currentPosition, initialRegion))
      },
      (error) => {
        console.log(error.code, error.message)
      },
      {
        enableHighAccuracy: false,
        timeout: 15000,
        maximumAge: 10000,
        distanceFilter: 0
      }
    )
  }
}

export {
  getStores,
  loadNearbyStore,
  updateCurrentPosition
}