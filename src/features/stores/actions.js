import * as actionTypes from './actionTypes'

function getStores() {
  const stores = []
  return {
    type: actionTypes.GET_STORES,
    stores
  }
}

function loadingNearbyStore() {
  return {
    type: actionTypes.LOADIND_NEARBY_STORE
  }
}

function loadedNearbyStore(store) {
  return {
    type: actionTypes.LOADED_NEARBY_STORE,
    store
  }
}

function updateCurrentPosition(currentPosition, initialRegion) {
  return {
    type: actionTypes.UPDATE_CURRENT_POSITION,
    currentPosition,
    initialRegion
  }
}

export {
  getStores,
  loadingNearbyStore,
  loadedNearbyStore,
  updateCurrentPosition
}