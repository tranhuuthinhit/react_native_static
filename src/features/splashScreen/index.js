import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  ImageBackground,
  Dimensions,
  Image,
  StatusBar
} from 'react-native'
import styles from './styles'
import { connect } from 'react-redux'
import navigationStyles from '../../constants/navigationStyles'
import { nextScreen } from './services'
import IMAGES from '../../assets/images'
import LogoImage from '../../components/logoImage'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import * as THEME from '../../constants/mainTheme'

export class SplashScreen extends Component {

  static propTypes = {
    dispatchNextScreen: PropTypes.func
  }

  static navigatorStyle = {
    ...navigationStyles.splashScreen
  }

  constructor(props) {
    super(props)
    const { height } = Dimensions.get('window')
    this.height = height
  }

  componentDidMount() {
    this.props.dispatchNextScreen()
  }

  render() {
    return (
      <KeyboardAwareScrollView
        style={styles.keyboardAwareScrollView}
      >
        <StatusBar barStyle='light-content' backgroundColor={THEME.PRIMARY_HEADER_COLOR} />
        <ImageBackground source={IMAGES.bg2} style={[styles.rootContainer, { height: this.height }]}>
          <LogoImage marginTop={40} />
          <Image
            source={IMAGES.bonus}
            style={styles.bonus}
          />
        </ImageBackground>
      </KeyboardAwareScrollView>
    )
  }
}

const mapDispatchToProps = {
  dispatchNextScreen: () => nextScreen()
}

export default connect(null, mapDispatchToProps)(SplashScreen)