import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  keyboardAwareScrollView: {
    flex: 1
  },
  rootContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  bonus: {
    marginTop: 40,
    width: 300,
    height: '60%',
    marginLeft: 35,
    resizeMode: 'contain'
  }
})

export default styles