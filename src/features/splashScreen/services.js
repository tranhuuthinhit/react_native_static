import * as mainActions from '../../app/services'
import * as actions from './actions'
import * as authActions from '../auth/services'
import * as ASYNC from '../../utils/asyncStorage'
import I18n from 'react-native-i18n'

function nextScreen() {
  return dispatch => {
    dispatch(actions.initialSplashScreen())
    dispatch(actions.loadingAppInfo())
    //ASYNC.clear()
    const appInfo = {
      deviceLanguage: I18n.currentLocale()
    }
    ASYNC.getAppInfo().then((appInfoStorage) => {
      if(appInfoStorage === null || appInfoStorage === undefined) {
        ASYNC.setAppInfo(appInfo)
      }
      ASYNC.getLoginStatus().then((loginStatus) => {
        if(loginStatus !== null && loginStatus !== undefined) {
          if(loginStatus === 'true') {
            ASYNC.getUserInfo().then((userProfileStorage) => {
              setTimeout(() => {
                if(userProfileStorage !== null && userProfileStorage !== undefined) {
                  dispatch(authActions.activeFromStorage(userProfileStorage))
                }
                dispatch(actions.loadedAppInfo())
                dispatch(mainActions.saveAppInfoToRootState(appInfo))
                dispatch(mainActions.updateAppRoot('home'))
              }, 2000)
            })
          } else {
            setTimeout(() => {
              dispatch(actions.loadedAppInfo())
              dispatch(mainActions.saveAppInfoToRootState(appInfo))
              dispatch(mainActions.updateAppRoot('home'))
            }, 2000)
          }
        } else {
          ASYNC.setLoginStatus('false')
          dispatch(actions.loadedAppInfo())
          dispatch(mainActions.saveAppInfoToRootState(appInfo))
          dispatch(mainActions.updateAppRoot('home'))
        }
      })
    })
  }
}

export {
  nextScreen
}