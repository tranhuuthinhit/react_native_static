import * as actionTypes from './actionTypes'

function initialSplashScreen() {
  return {
    type: actionTypes.INITIAL_SPLASHCREEN
  }
}

function loadingAppInfo() {
  return {
    type: actionTypes.LOADING_APP_INFO
  }
}

function loadedAppInfo() {
  return {
    type: actionTypes.LOADED_APP_INFO
  }
}

export {
  initialSplashScreen,
  loadingAppInfo,
  loadedAppInfo
}