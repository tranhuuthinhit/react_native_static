import * as types from './actionTypes'
import Immutable from 'seamless-immutable'

const initialState = Immutable({
  isLoadingAppInfo: false,
  isLoaddingAppInfo: false
})

export default function splashScreen(state = initialState, action = {}) {
  switch(action.type) {
    case types.INITIAL_SPLASHCREEN:
      return {
        ...state
      }
    case types.LOADING_APP_INFO:
      return {
        ...state,
        isLoadingAppInfo: true
      }
    case types.LOADED_APP_INFO:
      return {
        ...state,
        isLoaddingAppInfo: true,
        isLoadingAppInfo: false
      }
    default:
      return state
  }
}