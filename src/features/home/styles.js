import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingBottom: 20
  },
  viewService: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10
  }
})

export default styles