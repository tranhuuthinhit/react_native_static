import * as types from './actionTypes'
import Immutable from 'seamless-immutable'

const initialState = Immutable({
})

export default function home(state = initialState, action = {}) {

  switch(action.type) {
    case types.INITIAL_HOME_SCREEN:
      return {
        ...state
      }
    default:
      return state
  }
}