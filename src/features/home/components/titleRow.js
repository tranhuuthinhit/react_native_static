import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity
} from 'react-native'

export default class TitleRow extends Component {

  static propTypes = {
    title: PropTypes.string,
    titleWidth: PropTypes.number,
    isSubTitle: PropTypes.bool,
    marginTop: PropTypes.number,
    subTitle: PropTypes.string,
    subTitleOnPress: PropTypes.func
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.width = width
  }

  _renderSubTitle() {
    const { isSubTitle, subTitleOnPress, subTitle } = this.props
    if(isSubTitle) {
      return (
        <TouchableOpacity
          onPress={() => subTitleOnPress()}
        >
          <Text style={styles.subTitleText}>{subTitle}</Text>
        </TouchableOpacity>
      )
    }
  }

  render() {
    const { title, titleWidth, marginTop } = this.props
    return (
      <View style={[styles.titleView, { width: titleWidth, marginTop }]}>
        <Text style={styles.titleText}>{title.toUpperCase()}</Text>
        {this._renderSubTitle()}
      </View>
    )
  }
}

TitleRow.defaultProps = {
  title: '',
  titleWidth: 0,
  marginTop: 10,
  isSubTitle: false,
  subTitle: '',
  subTitleOnPress: () => {}
}

const styles = StyleSheet.create({
  titleView: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  titleText: {
    fontSize: 16,
    fontWeight: 'bold',
    color: '#F59220'
  },
  subTitleText: {
    fontSize: 12,
    color: '#F59220'
  }
})