import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet,
  View,
  Dimensions
} from 'react-native'
import Slideshow from 'react-native-slideshow'

export default class SliderBanner extends Component {

  static propTypes = {
    dataSource: PropTypes.array
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.width = width
  }

  render() {
    const { dataSource } = this.props
    return (
      <View style={styles.slideBanner}>
        <Slideshow
          dataSource={dataSource}
        />
      </View>
    )
  }
}

SliderBanner.defaultProps = {
  dataSource: []
}

const styles = StyleSheet.create({
  slideBanner: {
    backgroundColor: '#EBEBEB',
    height: 200,
    justifyContent: 'center',
    alignItems: 'center'
  }
})