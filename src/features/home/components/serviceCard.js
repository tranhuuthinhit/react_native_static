import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity
} from 'react-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

export default class ServiceCard extends Component {

  static propTypes = {
    title: PropTypes.string,
    iconName: PropTypes.string,
    backgroundColor: PropTypes.string,
    onPress: PropTypes.func
  }

  constructor(props) {
    super(props)
    const { width } = Dimensions.get('window')
    this.width = width
  }

  render() {
    const { iconName, backgroundColor, title, onPress } = this.props
    return (
      <TouchableOpacity
        onPress={() => onPress()}
      >
        <View style={[styles.cardView, { backgroundColor }]}>
          <Icon name={iconName} size={25} color='#FFFFFF' />
          <Text style={styles.titleText}>{title}</Text>
        </View>
      </TouchableOpacity>
    )
  }
}

ServiceCard.defaultProps = {
  title: '',
  iconName: '',
  backgroundColor: '',
  onPress: () => {}
}

const styles = StyleSheet.create({
  cardView: {
    width: 100,
    height: 80,
    borderRadius: 10,
    backgroundColor: '#F59220',
    overflow: 'hidden',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10
  },
  titleText: {
    fontSize: 11,
    fontWeight: 'bold',
    color: '#FFFFFF',
    marginTop: 10,
    flexWrap: 'wrap',
    textAlign: 'center'
  }
})