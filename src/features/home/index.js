import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Dimensions,
  StatusBar,
  ScrollView
} from 'react-native'
import styles from './styles'
import { connect } from 'react-redux'
import navigationStyles from '../../constants/navigationStyles'
import SliderBanner from './components/sliderBanner'
import TitleRow from './components/titleRow'
import ServiceCard from './components/serviceCard'
import StoreCard from '../../components/storeCard'
import PromotionCard from '../../components/promotionCard'
import I18n from '../../utils/i18n'
import Icon from 'react-native-vector-icons/Ionicons'
import * as ROUTES from '../../constants/routes'

import * as THEME from '../../constants/mainTheme'

export class Home extends Component {

  static propTypes = {
    navigator: PropTypes.object
  }

  static navigatorStyle = {
    ...navigationStyles.home
  }

  constructor(props) {
    super(props)
    const { width, height } = Dimensions.get('window')
    this.width = width
    this.height = height
    this._setNavBarButtons()
    this.props.navigator.setOnNavigatorEvent(this._onNavigatorEvent.bind(this))
  }

  async prepareIcons() {
    const icons = await Promise.all([
      Icon.getImageSource('md-alert', 25)
    ])
    const [ alert ] = icons
    return { alert }
  }

  _onNavigatorEvent(event) {
    if(event.type === 'NavBarButtonPress') {
      if(event.id === 'alert') {
        this._goToOtherScreen(ROUTES.SETTING, I18n('settingScreen.title'))
      }
    }
  }

  async _setNavBarButtons() {
    const icons = await this.prepareIcons()
    this.props.navigator.setButtons({
      leftButtons: [
        {
          title: 'About',
          icon: icons.alert,
          id: 'alert'
        }
      ],
      animated: true
    })
  }

  _goToOtherScreen(screen, title) {
    this.props.navigator.push({
      screen,
      title,
      backButtonTitle: ''
    })
  }

  render() {
    return (
      <ScrollView>
        <View style={styles.rootContainer}>
          <StatusBar barStyle='light-content' backgroundColor={THEME.PRIMARY_HEADER_COLOR} />
          <SliderBanner
            dataSource={[
              { url:'http://demo.satra.com.vn/upload/large/news/satrafoods-daidien.jpg' },
              { url:'https://nld.mediacdn.vn/thumb_w/640/2018/2/8/4-15180634643841790793893.jpg' },
              { url:'http://www.satra.com.vn/upload/large//Tin%20SATRA/Thang%2042017/khuyen-mai.jpg' }
            ]}
          />

          <TitleRow
            title={I18n('servicesScreen.title').toUpperCase()}
            titleWidth={this.width - 40}
            isSubTitle={true}
            subTitle={I18n('homeScreen.allButtonText')}
            marginTop={20}
          />

          <View style={[styles.viewService, { width: this.width - 40 }]}>
            <ServiceCard
              title={I18n('homeScreen.servicesButton.deliveryButtonText')}
              iconName='shopping-basket'
              backgroundColor='#F59220'
            />
            <ServiceCard
              title={I18n('homeScreen.servicesButton.giftCardButtonText')}
              iconName='redeem'
              backgroundColor='#8BC540'
            />
            <ServiceCard
              title={I18n('homeScreen.servicesButton.orderButtonText')}
              iconName='call'
              backgroundColor='#ED1D25'
            />
          </View>

          <TitleRow
            title={I18n('homeScreen.nearbyStore').toUpperCase()}
            titleWidth={this.width - 40}
            isSubTitle={true}
            subTitle={I18n('homeScreen.allButtonText')}
            marginTop={30}
          />

          <StoreCard
            storeName='SATRA FOODS PHÚ THUẬN'
            storeAddress='35 Phú Thuận, P.Phú Thuận, Q7, Tp.HCM'
            storeImage='http://thuonggiaonline.vn/upload/images/2016/12/Satrafood-1.jpg'
            cardWidth={this.width - 40}
            isIconDisplay={true}
            iconName='directions'
            iconSize={25}
            iconColor='#FFFFFF'
            isDisplayAvatar={true}
          />

          <TitleRow
            title={I18n('homeScreen.promotion').toUpperCase()}
            titleWidth={this.width - 40}
            isSubTitle={true}
            subTitle={I18n('homeScreen.allButtonText')}
            marginTop={30}
          />

          <PromotionCard
            title='Giảm giá 50% sản phẩm dầu thực vật'
            promotionImage='http://demo.satra.com.vn/upload/large/news/satrafoods-daidien.jpg'
            cardWidth={this.width - 40}
          />

        </View>
      </ScrollView>
    )
  }
}

const mapDispatchToProps = {}

export default connect(null, mapDispatchToProps)(Home)