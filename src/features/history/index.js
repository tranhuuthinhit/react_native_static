import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  View,
  Text,
  Dimensions
} from 'react-native'
import styles from './styles'
import { connect } from 'react-redux'
import navigationStyles from '../../constants/navigationStyles'

export class MyHistory extends Component {

  static propTypes = {
  }

  static navigatorStyle = {
    ...navigationStyles.setting
  }

  constructor(props) {
    super(props)
    const { width, height } = Dimensions.get('window')
    this.width = width
    this.height = height
  }

  render() {
    return (
      <View style={styles.rootContainer}>
        <View
          style={[styles.content, { width: this.width, height: this.height }]}
        >
          <View style={styles.textZone}>
            <Text style={styles.slideTitle}>History Screen</Text>
          </View>
        </View>
      </View>
    )
  }
}

export default connect()(MyHistory)