import { StyleSheet } from 'react-native'
import * as THEME from '../../constants/mainTheme'

const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: THEME.PRIMARY_BACKGROUND_COLOR,
    flexDirection: 'column'
  },
  content:{
    alignItems: 'center',
    flexDirection: 'column',
    backgroundColor: THEME.PRIMARY_BACKGROUND_COLOR
  }
})

export default styles