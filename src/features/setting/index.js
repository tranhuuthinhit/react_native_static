import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {
  Dimensions,
  ScrollView,
  StatusBar,
  View
} from 'react-native'
import { connect } from 'react-redux'
import navigationStyles from '../../constants/navigationStyles'
import ProfileTextRowAction from '../../components/profileRowAction'
import ProfileTextRowWithSwitch from '../../components/profileRowWithSwitch'
import * as ROUTES from '../../constants/routes'
import I18n from '../../utils/i18n'
import styles from './styles'

export class Setting extends Component {

  static propTypes = {
    navigator: PropTypes.object
  }

  static navigatorStyle = {
    ...navigationStyles.setting
  }

  constructor(props) {
    super(props)
    const { width, height } = Dimensions.get('window')
    this.width = width
    this.height = height
    this.state = {
      switchStatus: false
    }
  }

  _changeStatusNotification(value) {
    this.setState({
      switchStatus: value
    })
  }

  _goToOtherScreen(screen, title) {
    this.props.navigator.push({
      screen,
      title,
      backButtonTitle: ''
    })
  }

  render() {
    const { switchStatus } = this.state
    return (
      <ScrollView>
        <StatusBar barStyle='light-content' />
        <View style={styles.rootContainer}>
          <ProfileTextRowWithSwitch
            iconName={switchStatus ? 'md-notifications' : 'md-notifications-off'}
            text={I18n('settingScreen.getNotification')}
            value={switchStatus}
            onValueChange={(value) => this._changeStatusNotification(value)}
          />
          <ProfileTextRowAction
            iconName='md-people'
            onPress={() => this._goToOtherScreen(ROUTES.ABOUT, I18n('aboutScreen.title'))}
            text={I18n('settingScreen.aboutUs')}
          />
          <ProfileTextRowAction
            iconName='md-lock'
            onPress={() => this._goToOtherScreen(ROUTES.PRIVACY, I18n('privacyScreen.title'))}
            text={I18n('settingScreen.privacy')}
          />
          <ProfileTextRowAction
            iconName='md-clipboard'
            onPress={() => this._goToOtherScreen(ROUTES.TERM, I18n('termScreen.title'))}
            text={I18n('settingScreen.term')}
          />
        </View>
      </ScrollView>
    )
  }
}

export default connect()(Setting)