import * as THEME from './mainTheme'

const naviStyleStatic = {
  navBarBackgroundColor: THEME.PRIMARY_COLOR,
  statusBarTextColorScheme: 'light',
  statusBarColor: THEME.PRIMARY_COLOR,
  navBarTextColor: '#FFFFFF',
  navBarButtonColor: '#FFFFFF',
  navBarTranslucent: false,
  topBarElevationShadowEnabled: false,
  navBarNoBorder: true
}

const navigationStyles = {
  splashScreen: {
    ...naviStyleStatic,
    navBarHidden: true
  },
  signIn: {
    ...naviStyleStatic,
    navBarHidden: false
  },
  signUp: {
    ...naviStyleStatic,
    navBarHidden: false
  },
  myCard: {
    ...naviStyleStatic,
    navBarHidden: false
  },
  store: {
    ...naviStyleStatic,
    navBarHidden: false
  },
  home: {
    ...naviStyleStatic,
    navBarHidden: false
  },
  user: {
    ...naviStyleStatic,
    navBarHidden: true
  },
  myInformation: {
    ...naviStyleStatic,
    navBarHidden: false
  },
  setting: {
    ...naviStyleStatic
  }
}

export default navigationStyles