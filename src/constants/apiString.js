export const kSERVER = 'http://loyaltycard.satra.com.vn:8686/api/'
export const kSERVERV3 = 'http://loyaltycard.satra.com.vn:8686/apiv3/'
export const kSERVER_TEST = 'http://loyaltycard.satra.com.vn:8686/apibackup'

export const kSATRA_SERVER = 'http://loyaltycard.satra.com.vn:8686/apibackup'

export const kAPI_CHECKACCOUNT = 'CheckAccount?info='
export const KAPI_SENDOTP = 'SendOTP?info='
export const kAPI_SIGNIN = 'Login?info='
export const kAPI_SIGNINV3 = 'Login?info=%@&passwordHash=%@'

export const kAPI_TRANSACTION = 'Transactions?session='
export const kAPI_TRANSACTION_DETAIL = 'TransactionDetails?session=%@&TransactionId=%@&ReceiptId=%@&StoreId=%@&TerminalId=%@'

export const kAPI_STORES = 'Stores'
export const kAPI_MONEY = 'Balance?session='
export const kAPI_SIGNUP = 'SignUp'
export const kAPI_SIGNOUT = 'SignOut?session='
export const kAPI_SIGNOTHERSOUT = 'SignOthersOut?session='
export const kAPI_INBOX = 'Inbox?session='
export const kAPI_REMINDER = 'TestReminder?session='

export const kAPI_PROVINCES = 'http://loyaltycard.satra.com.vn:8686/unitad/Province'
export const kAPI_DISTRICTS = 'District?id='
export const kAPI_WARDS = 'ward?id='
export const kAPI_INTRODUCE = 'Introduce?session='
export const kAPI_UPDATE_USER_INFO = 'UpdateLoyaltyInfo'

export const kCONFIRM_OTP = 'confirm?info=%@&otp=%@&passwordHash=%@'
export const kUSER_INFO = 'userinformation?session='
export const kUPDATE_DEVICE_TOKEN = 'UpdateDeviceID?session=%@&deviceId=%@&deviceType=1'

export const kAPI_POLICY = 'Policy?session='
export const kAPI_TERM_OF_USE = 'TermofUse?session='

export const kGOOGLE_GEOCODE_URL = 'https://maps.googleapis.com/maps/api/geocode/json?address=%@&key=%@'
export const kAPI_RequestPasswordResetOTP = 'RequestPasswordResetOTP?info='
export const kAPI_ResetPassword = 'ResetPassword?info=%@&passwordResetOTP=%@&newPasswordHash=%@'
export const kAPI_ChangePassword = 'ChangePassword?info=%@&oldPasswordHash=%@&newPasswordHash=%@'
