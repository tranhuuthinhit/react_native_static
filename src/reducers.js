import { createStore, applyMiddleware, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import { createLogger } from 'redux-logger'
import root from './app/reducers'
import auth from './features/auth/reducers'
import user from './features/user/reducers'
import splashScreen from './features/splashScreen/reducers'
import stores from './features/stores/reducers'

const loggerMiddleware = createLogger()
const reducer = combineReducers({
  root,
  auth,
  user,
  splashScreen,
  stores
})

const store = createStore(
  reducer,
  applyMiddleware(
    thunk,
    loggerMiddleware
  )
)

export {
  store
}